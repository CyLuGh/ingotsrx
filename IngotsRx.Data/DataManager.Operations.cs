﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SQLite;
using System.Linq;
using System.Text;

namespace IngotsRx.Data
{
    partial class DataManager
    {
        public IEnumerable<Transaction> GetTransactions()
        {
            using (var cn = new SQLiteConnection(ConnectionString))
            {
                cn.Open();

                using (var cm = cn.CreateCommand())
                {
                    cm.CommandText = @"SELECT id, id_account, date, description, value, category, subcat, shop, executed FROM Transactions
                                        WHERE id_account IN ( SELECT id FROM Accounts WHERE isdeleted = 0 )";
                    using (var rdr = cm.ExecuteReader())
                        while (rdr.Read())
                        {
                            int i = -1;
                            yield return new Transaction
                                         {
                                             Id = Convert.ToUInt64(rdr.GetInt64(++i)),
                                             AccountId = rdr.GetInt64(++i),
                                             Date = rdr.GetDateTime(++i),
                                             Description = !rdr.IsDBNull(++i) ? rdr.GetString(i) : string.Empty,
                                             Value = rdr.GetDouble(++i),
                                             Category = !rdr.IsDBNull(++i) ? rdr.GetString(i) : "None",
                                             SubCategory = !rdr.IsDBNull(++i) ? rdr.GetString(i) : "None",
                                             Shop = !rdr.IsDBNull(++i) ? rdr.GetString(i) : string.Empty,
                                             IsExecuted = rdr.GetBoolean(++i)
                                         };
                        }
                }
            }
        }

        public IEnumerable<Transfer> GetTransfers()
        {
            using (var cn = new SQLiteConnection(ConnectionString))
            {
                cn.Open();

                using (var cm = cn.CreateCommand())
                {
                    cm.CommandText = @"SELECT id, id_account, id_target, date, description, value, executed FROM Transfers
                                        WHERE id_account IN ( SELECT id FROM Accounts WHERE isdeleted = 0 )";
                    using (var rdr = cm.ExecuteReader())
                        while (rdr.Read())
                        {
                            int i = -1;
                            yield return new Transfer
                                         {
                                             Id = Convert.ToUInt64(rdr.GetInt64(++i)),
                                             AccountId = rdr.GetInt64(++i),
                                             TargetId = rdr.GetInt64(++i),
                                             Date = rdr.GetDateTime(++i),
                                             Description = !rdr.IsDBNull(++i) ? rdr.GetString(i) : string.Empty,
                                             Value = rdr.GetDouble(++i),
                                             IsExecuted = rdr.GetBoolean(++i)
                                         };
                        }
                }
            }
        }

        public IEnumerable<Operation> GetOperations()
        {
            using (var cn = new SQLiteConnection(ConnectionString))
            {
                cn.Open();

                using (var cm = cn.CreateCommand())
                {
                    cm.CommandText = @"SELECT * FROM (
                                        SELECT 0 AS Type, id, id_account AS acc, -1 as target, date, description, value, category, subcat, shop, executed FROM Transactions
                                        UNION
                                        SELECT 1 AS Type, id, id_account AS acc, id_target as target, date, description, value, NULL AS category, NULL AS subcat, NULL AS shop, executed FROM Transfers
                                        ) WHERE acc IN ( SELECT id FROM Accounts WHERE isdeleted = 0 )";

                    using (var rdr = cm.ExecuteReader())
                        while (rdr.Read())
                        {
                            var kind = (OperationKind) rdr.GetInt32(0);
                            switch (kind)
                            {
                                case OperationKind.Transaction:
                                    yield return new Transaction
                                                 {
                                                     Id = Convert.ToUInt64(rdr.GetInt64(1)),
                                                     AccountId = rdr.GetInt64(2),
                                                     Date = rdr.GetDateTime(4),
                                                     Description = !rdr.IsDBNull(5) ? rdr.GetString(5) : string.Empty,
                                                     Value = rdr.GetDouble(6),
                                                     Category = !rdr.IsDBNull(7) ? rdr.GetString(7) : "None",
                                                     SubCategory = !rdr.IsDBNull(8) ? rdr.GetString(8) : "None",
                                                     Shop = !rdr.IsDBNull(9) ? rdr.GetString(9) : string.Empty,
                                                     IsExecuted = rdr.GetBoolean(10)
                                                 };
                                    break;

                                case OperationKind.Transfer:
                                    var transfer = new Transfer
                                                   {
                                                       Id = Convert.ToUInt64(rdr.GetInt64(1)),
                                                       AccountId = rdr.GetInt64(2),
                                                       TargetId = rdr.GetInt64(3),
                                                       Date = rdr.GetDateTime(4),
                                                       Description = !rdr.IsDBNull(5) ? rdr.GetString(5) : string.Empty,
                                                       Value = rdr.GetDouble(6),
                                                       IsExecuted = rdr.GetBoolean(10)
                                                   };
                                    yield return transfer;
                                    yield return transfer.OppositeTransfer();
                                    break;
                            }
                        }
                }
            }
        }

        #region Inserts

        public void InsertOperations(params Operation[] operations)
        {
            InsertTransactions(operations.OfType<Transaction>());
            InsertTransfers(operations.OfType<Transfer>());
        }

        private void InsertTransactions(IEnumerable<Transaction> transactions)
        {
            using (var cn = new SQLiteConnection(ConnectionString))
            {
                cn.Open();
                using (var tr = cn.BeginTransaction())
                {
                    try
                    {
                        using (var cm = cn.CreateCommand())
                        {
                            cm.Transaction = tr;

                            cm.CommandText = @"INSERT INTO Transactions(id,id_account,date,description,value,category,subcat,shop,executed)
                                                VALUES (@i,@a,@t,@d,@v,@c,@s,@h,@e)";
                            
                            cm.Parameters.Add("@i", DbType.UInt64);
                            cm.Parameters.Add("@a", DbType.Int64);   
                            cm.Parameters.Add("@t", DbType.Date);    
                            cm.Parameters.Add("@d", DbType.String);  
                            cm.Parameters.Add("@v", DbType.Double);
                            cm.Parameters.Add("@c", DbType.String);
                            cm.Parameters.Add("@s", DbType.String);
                            cm.Parameters.Add("@h", DbType.String);
                            cm.Parameters.Add("@e", DbType.Byte);    

                            foreach (var transaction in transactions)
                            {
                                cm.Parameters["@i"].Value = transaction.Id;
                                cm.Parameters["@a"].Value = transaction.AccountId;
                                cm.Parameters["@t"].Value = transaction.Date;
                                cm.Parameters["@d"].Value = transaction.Description;
                                cm.Parameters["@v"].Value = transaction.Value;
                                cm.Parameters["@c"].Value = !string.IsNullOrWhiteSpace(transaction.Category) ? transaction.Category : "None";
                                cm.Parameters["@s"].Value = !string.IsNullOrWhiteSpace(transaction.SubCategory) ? transaction.SubCategory : "None";
                                cm.Parameters["@h"].Value = transaction.Shop;
                                cm.Parameters["@e"].Value = transaction.IsExecuted ? 1 : 0;
                                cm.ExecuteNonQuery();
                            }
                            
                            tr.Commit();
                        }
                    }
                    catch (Exception)
                    {
                        tr.Rollback();
                        throw;
                    }
                }
            }
        }

        private void InsertTransfers(IEnumerable<Transfer> transfers)
        {
            using (var cn = new SQLiteConnection(ConnectionString))
            {
                cn.Open();
                using (var tr = cn.BeginTransaction())
                {
                    try
                    {
                        using (var cm = cn.CreateCommand())
                        {
                            cm.Transaction = tr;

                            cm.CommandText = @"INSERT INTO Transfers(id,id_account,date,description,value,id_target,executed)
                                                VALUES (@i,@a,@t,@d,@v,@tgt,@e)";

                            cm.Parameters.Add("@i", DbType.UInt64);
                            cm.Parameters.Add("@a", DbType.Int64);   
                            cm.Parameters.Add("@t", DbType.Date);    
                            cm.Parameters.Add("@d", DbType.String);  
                            cm.Parameters.Add("@v", DbType.Double);  
                            cm.Parameters.Add("@tgt", DbType.Int64); 
                            cm.Parameters.Add("@e", DbType.Byte);    

                            foreach (var transfer in transfers)
                            {
                                cm.Parameters["@i"].Value = transfer.Id;
                                cm.Parameters["@a"].Value = transfer.AccountId;
                                cm.Parameters["@t"].Value = transfer.Date;
                                cm.Parameters["@d"].Value = transfer.Description;
                                cm.Parameters["@v"].Value = transfer.Value;
                                cm.Parameters["@tgt"].Value = transfer.TargetId;
                                cm.Parameters["@e"].Value = transfer.IsExecuted ? 1 : 0;
                                cm.ExecuteNonQuery();
                            }

                            tr.Commit();
                        }
                    }
                    catch (Exception)
                    {
                        tr.Rollback();
                        throw;
                    }
                }
            }
        }

        #endregion Inserts

        #region Updates

        public void UpdateOperation(Operation operation)
        {
            switch (operation)
            {
                case Transaction transaction:
                    UpdateTransaction(transaction);
                    return;

                case Transfer transfer:
                    UpdateTransfer(transfer);
                    return;
            }

            throw new ApplicationException("Couldn't determine if operation was a transaction or a transfer!");
        }

        private void UpdateTransaction(Transaction transaction)
        {
            using (var cn = new SQLiteConnection(ConnectionString))
            {
                cn.Open();
                using (var tr = cn.BeginTransaction())
                {
                    try
                    {
                        using (var cm = cn.CreateCommand())
                        {
                            cm.Transaction = tr;
                            cm.CommandText = @"UPDATE Transactions SET
                                                        date = @t,
                                                        description = @d,
                                                        value = @v,
                                                        category = @c,
                                                        subcat = @s,
                                                        shop = @h,
                                                        executed = @e
                                                    WHERE id = @i";
                            cm.Parameters.AddWithValue("@t", transaction.Date);
                            cm.Parameters.AddWithValue("@d", transaction.Description);
                            cm.Parameters.AddWithValue("@v", transaction.Value);
                            cm.Parameters.AddWithValue("@c", transaction.Category);
                            cm.Parameters.AddWithValue("@s", transaction.SubCategory);
                            cm.Parameters.AddWithValue("@h", transaction.Shop);
                            cm.Parameters.AddWithValue("@i", transaction.Id);
                            cm.Parameters.AddWithValue("@e", transaction.IsExecuted ? 1 : 0);

                            cm.ExecuteNonQuery();
                            tr.Commit();
                        }
                    }
                    catch (Exception)
                    {
                        tr.Rollback();
                        throw;
                    }
                }
            }
        }

        private void UpdateTransfer(Transfer transfer)
        {
            using (var cn = new SQLiteConnection(ConnectionString))
            {
                cn.Open();
                using (var tr = cn.BeginTransaction())
                {
                    try
                    {
                        using (var cm = cn.CreateCommand())
                        {
                            cm.Transaction = tr;
                            cm.CommandText = @"UPDATE Transfers SET
                                                        date = @t,
                                                        description = @d,
                                                        value = @v,
                                                        id_account = @a,
                                                        id_target = @g,
                                                        executed = @e
                                                    WHERE id = @i";
                            cm.Parameters.AddWithValue("@t", transfer.Date);
                            cm.Parameters.AddWithValue("@d", transfer.Description);
                            cm.Parameters.AddWithValue("@v", transfer.Value);
                            cm.Parameters.AddWithValue("@a", transfer.AccountId);
                            cm.Parameters.AddWithValue("@g", transfer.TargetId);
                            cm.Parameters.AddWithValue("@i", transfer.Id);
                            cm.Parameters.AddWithValue("@e", transfer.IsExecuted ? 1 : 0);

                            cm.ExecuteNonQuery();
                            tr.Commit();
                        }
                    }
                    catch (Exception)
                    {
                        tr.Rollback();
                        throw;
                    }
                }
            }
        }

        #endregion Updates

        #region Deletes

        public void DeleteOperation(Operation operation)
        {
            switch (operation)
            {
                case Transaction transaction:
                    DeleteTransaction(transaction);
                    return;

                case Transfer transfer:
                    DeleteTransfer(transfer);
                    return;
            }

            throw new ArgumentException("Operation has no valid implementation!");
        }

        private void DeleteTransaction(Transaction transaction)
        {
            if (transaction == null)
                throw new ArgumentNullException(nameof(transaction));

            using (var cn = new SQLiteConnection(ConnectionString))
            {
                cn.Open();
                using (var cm = cn.CreateCommand())
                {
                    cm.CommandText = @"DELETE FROM Transactions WHERE Id = @id";
                    cm.Parameters.AddWithValue("@id", transaction.Id);
                    cm.ExecuteNonQuery();
                }
            }
        }

        private void DeleteTransfer(Transfer transfer)
        {
            if (transfer == null)
                throw new ArgumentNullException(nameof(transfer));

            using (var cn = new SQLiteConnection(ConnectionString))
            {
                cn.Open();
                using (var cm = cn.CreateCommand())
                {
                    cm.CommandText = @"DELETE FROM Transfers WHERE Id = @id";
                    cm.Parameters.AddWithValue("@id", transfer.Id);
                    cm.ExecuteNonQuery();
                }
            }
        }

        #endregion Deletes
    }
}