﻿namespace IngotsRx.Data
{
    public enum AccountKind { Checking = 0, Saving = 1, Locked = 2 }

    public class Account
    {
        public string Bank { get; set; } = "Unknown";
        public string Bic { get; set; } = "Unknown";
        public string Description { get; set; } = "Unknown";
        public string Iban { get; set; }
        public long Id { get; set; }
        public AccountKind Kind { get; set; }
        public double StartValue { get; set; }
        public string Stash { get; set; } = "None";
    }
}