﻿using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.Text;

namespace IngotsRx.Data
{
    partial class DataManager
    {
        public IEnumerable<Account> GetAccounts()
        {
            using (var cn = new SQLiteConnection(ConnectionString))
            {
                cn.Open();

                using (var cm = cn.CreateCommand())
                {
                    cm.CommandText = @"SELECT id, iban, bic, description, startvalue, kind, stash, bank FROM Accounts WHERE isdeleted = 0 ORDER BY bank, kind";
                    using (var rdr = cm.ExecuteReader())
                        while (rdr.Read())
                            yield return new Account
                            {
                                Id = rdr.GetInt32(0),
                                Iban = rdr.GetString(1),
                                Bic = rdr.GetString(2),
                                Description = rdr.GetString(3),
                                StartValue = rdr.GetDouble(4),
                                Kind = (AccountKind)rdr.GetInt32(5),
                                Stash = rdr.GetString(6),
                                Bank = rdr.GetString(7)
                            };
                }
            }
        }

        public int InsertAccount(Account account)
        {
            using (var cn = new SQLiteConnection(ConnectionString))
            {
                cn.Open();
                using (var tr = cn.BeginTransaction())
                {
                    try
                    {
                        using (var cm = cn.CreateCommand())
                        {
                            cm.Transaction = tr;

                            // Retrieve version
                            cm.CommandText = @"SELECT MAX(id) FROM Accounts";
                            object o = cm.ExecuteScalar();
                            int ver = 0;
                            if (o != DBNull.Value)
                                ver = Convert.ToInt32(o);

                            cm.CommandText = @"INSERT INTO Accounts(id,iban,bic,description,startvalue,isdeleted,kind,stash,bank)
                                                VALUES (@i,@iban,@bic,@desc,@sv,@del,@k,@stash,@bank)";
                            cm.Parameters.AddWithValue("@i", ++ver);
                            cm.Parameters.AddWithValue("@iban", IbanUtils.UnFormat(account.Iban));
                            cm.Parameters.AddWithValue("@bic", !string.IsNullOrWhiteSpace(account.Bic) ? account.Bic : "Unknown");
                            cm.Parameters.AddWithValue("@desc", !string.IsNullOrWhiteSpace(account.Description) ? account.Description : "Unknown");
                            cm.Parameters.AddWithValue("@sv", account.StartValue);
                            cm.Parameters.AddWithValue("@del", false);
                            cm.Parameters.AddWithValue("@k", (int)account.Kind);
                            cm.Parameters.AddWithValue("@stash", !string.IsNullOrWhiteSpace(account.Stash) ? account.Stash : "None");
                            cm.Parameters.AddWithValue("@bank", !string.IsNullOrWhiteSpace(account.Bank) ? account.Bank : "Unknown");
                            cm.ExecuteNonQuery();

                            tr.Commit();
                            return ver;
                        }
                    }
                    catch (Exception)
                    {
                        tr.Rollback();
                        throw;
                    }
                }
            }
        }

        public void UpdateAccount(Account account)
        {
            using (var cn = new SQLiteConnection(ConnectionString))
            {
                cn.Open();
                using (var tr = cn.BeginTransaction())
                {
                    try
                    {
                        using (var cm = cn.CreateCommand())
                        {
                            cm.Transaction = tr;
                            cm.CommandText = @"UPDATE Accounts SET iban = @iban, bic = @bic, description = @desc, startvalue = @sv, kind = @kind, stash = @stash, bank = @bank WHERE id = @id";

                            cm.Parameters.AddWithValue("@iban", IbanUtils.UnFormat(account.Iban));
                            cm.Parameters.AddWithValue("@bic", !string.IsNullOrWhiteSpace(account.Bic) ? account.Bic : "Unknown");
                            cm.Parameters.AddWithValue("@desc", !string.IsNullOrWhiteSpace(account.Description) ? account.Description : "Unknown");
                            cm.Parameters.AddWithValue("@sv", account.StartValue);
                            cm.Parameters.AddWithValue("@kind", account.Kind);
                            cm.Parameters.AddWithValue("@stash", !string.IsNullOrWhiteSpace(account.Stash) ? account.Stash : "None");
                            cm.Parameters.AddWithValue("@bank", !string.IsNullOrWhiteSpace(account.Bank) ? account.Bank : "Unknown");
                            cm.Parameters.AddWithValue("@id", account.Id);

                            cm.ExecuteNonQuery();
                            tr.Commit();
                        }
                    }
                    catch (Exception)
                    {
                        tr.Rollback();
                        throw;
                    }
                }
            }
        }
    }
}