﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IngotsRx.Data
{
    public enum OperationKind
    {
        Transaction, Transfer
    }

    public abstract class Operation : ICloneable
    {
        public ulong Id { get; set; }
        public long AccountId { get; set; }
        public DateTime Date { get; set; }
        public double Value { get; set; }
        public string Description { get; set; }
        public bool IsExecuted { get; set; }

        public object Clone() => MemberwiseClone();
    }

    public class Transaction : Operation, IEquatable<Transaction>
    {
        public string Category { get; set; }
        public string SubCategory { get; set; }
        public string Shop { get; set; }

        public bool Equals(Transaction other)
        {
            if (other != null)
                return Id.Equals(other.Id) && AccountId.Equals(other.AccountId);

            return false;
        }

        public override bool Equals(object obj) => Equals(obj as Transaction);

        public override int GetHashCode()
        {
            return Id.GetHashCode() ^ AccountId.GetHashCode();
        }
    }

    public class Transfer : Operation, IEquatable<Transfer>
    {
        public long TargetId { get; set; }
        public bool IsDerived { get; set; }

        public Transfer OppositeTransfer()
        {
            var opposite = Clone() as Transfer;
            opposite.Value = Value * -1;
            opposite.AccountId = TargetId;
            opposite.TargetId = AccountId;
            opposite.IsDerived = true;
            return opposite;
        }

        public bool Equals(Transfer other)
        {
            if (other != null)
                return Id.Equals(other.Id) && AccountId.Equals(other.AccountId) && TargetId.Equals(other.TargetId) &&
                       IsDerived.Equals(other.IsDerived);

            return false;
        }

        public override bool Equals(object obj) => Equals(obj as Transfer);

        public override int GetHashCode()
        {
            return Id.GetHashCode() ^ AccountId.GetHashCode() ^ TargetId.GetHashCode() ^ IsDerived.GetHashCode();
        }
    }
}