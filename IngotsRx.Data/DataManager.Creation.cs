﻿using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.Text;

namespace IngotsRx.Data
{
    partial class DataManager
    {
        public void CreateDatabase(string path)
        {
            using (var cn = new SQLiteConnection($"Data Source={path};Version=3;New=true;Compress=true"))
            {
                cn.Open();

                using (var tr = cn.BeginTransaction())
                {
                    try
                    {
                        using (var cm = cn.CreateCommand())
                        {
                            cm.Transaction = tr;

                            cm.CommandText = AccountsCreationQuery;
                            cm.ExecuteNonQuery();

                            cm.CommandText = TransactionsCreationQuery;
                            cm.ExecuteNonQuery();

                            cm.CommandText = TransfersCreationQuery;
                            cm.ExecuteNonQuery();
                        }

                        tr.Commit();
                    }
                    catch (Exception)
                    {
                        tr.Rollback();
                        throw;
                    }
                }
            }
        }

        #region Creation queries

        public string AccountsCreationQuery => @"CREATE TABLE Accounts (id INTEGER NOT NULL PRIMARY KEY ,iban VARCHAR(50) UNIQUE
                                    ,bic VARCHAR(20)
                                    ,description VARCHAR(50)
                                    ,startvalue DECIMAL(16,2)
                                    ,isdeleted INTEGER NOT NULL DEFAULT 0
                                    ,kind INTEGER NOT NULL DEFAULT 0
                                    ,stash VARCHAR(20) NOT NULL DEFAULT 'None'
                                    ,bank VARCHAR(50) NOT NULL DEFAULT 'Unknown'
                                    )";

        public string TransactionsCreationQuery => @"CREATE TABLE Transactions
                                    (id BIGINT NOT NULL PRIMARY KEY
                                    ,id_account INTEGER
                                    ,date DATETIME
                                    ,description VARCHAR(128)
                                    ,value DECIMAL(16,2)
                                    ,category VARCHAR(64)
                                    ,subcat VARCHAR(64)
                                    ,shop VARCHAR(64)
                                    ,executed BIT)";

        public string TransfersCreationQuery => @"CREATE TABLE Transfers
                                    (id BIGINT NOT NULL PRIMARY KEY
                                    ,id_account INTEGER
                                    ,date DATETIME
                                    ,description VARCHAR(128)
                                    ,value DECIMAL(16,2)
                                    ,id_target INTEGER
                                    ,executed BIT)";

        #endregion Creation queries
    }
}