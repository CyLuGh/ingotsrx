﻿using System;
using System.Data.SQLite;

namespace IngotsRx.Data
{
    public sealed partial class DataManager
    {
        public static DataManager Instance { get; } = new DataManager();

        public string Path { get; set; }

        private string ConnectionString => $"Data Source ={Path}; Version=3;New=true;Compress=true";

        private DataManager()
        {
        }
    }
}