﻿using DynamicData;
using IngotsRx.ViewModels;
using MaterialDesignExtensions.Controls;
using MaterialDesignThemes.Wpf;
using ReactiveUI;
using Splat;
using System;
using System.Reactive;
using System.Reactive.Disposables;
using System.Reactive.Linq;
using System.Windows.Controls;

namespace IngotsRx
{
    public partial class MainWindow : IViewFor<IngotsViewModel>
    {
        public MainWindow()
        {
            InitializeComponent();

            this.WhenActivated(disposables =>
            {
                this.WhenAnyValue(v => v.ViewModel)
                    .WhereNotNull()
                    .Do(x => PopulateFromViewModel(x, disposables))
                    .Subscribe()
                    .DisposeWith(disposables);
            });

            _snackbar.MessageQueue = MessageQueue;
            var ivm = new IngotsViewModel();
            ivm.FillDemo();

            ViewModel = ivm;
        }

        private void PopulateFromViewModel(IngotsViewModel ingotsViewModel, CompositeDisposable disposables)
        {
            ingotsViewModel.LoggingViewModel.ShowMessageInteraction.RegisterHandler(context =>
                {
                    MessageQueue.Enqueue(context.Input.Message);
                    context.SetOutput(Unit.Default);
                })
                .DisposeWith(disposables);

            this.OneWayBind(ingotsViewModel,
                vm => vm.NavigationItems,
                v => v.NavigationRail.Items)
                .DisposeWith(disposables);

            this.Bind(ingotsViewModel,
                vm => vm.SelectedNavigationItem,
                v => v.NavigationRail.SelectedItem)
                .DisposeWith(disposables);

            this.OneWayBind(ingotsViewModel,
                vm => vm.Router,
                v => v.RoutedViewHost.Router)
               .DisposeWith(disposables);

            this.OneWayBind(ingotsViewModel,
                vm => vm.LoggingViewModel,
                v => v.LogsViewHost.Content,
                vm =>
                {
                    var viewLocator = Locator.Current.GetService<IViewLocator>();
                    var view = viewLocator.ResolveView(vm);
                    view.ViewModel = vm;
                    return view;
                })
             .DisposeWith(disposables);

            _bLogs.Events().PreviewMouseUp.SubscribeSafe(_ => _hamburgerToggle.IsChecked = false).DisposeWith(disposables);

            this.BindCommand(ingotsViewModel,
                vm => vm.PickDatabaseCommand,
                v => v._bCreateDatabase,
                Observable.Return(true))
                .DisposeWith(disposables);

            this.BindCommand(ingotsViewModel,
                vm => vm.PickDatabaseCommand,
                v => v._bLoadDatabase,
                Observable.Return(false))
                .DisposeWith(disposables);

            ingotsViewModel.PickDatabaseInteraction.RegisterHandler(async context =>
                {
                    var filePath = string.Empty;
                    if (context.Input)
                    {
                        var dialogArgs = new SaveFileDialogArguments()
                        {
                            Width = 800,
                            Height = 600,
                            CreateNewDirectoryEnabled = true,
                            Filters = "SQLite files|*.db3;*.db"
                        };
                        var result = await SaveFileDialog.ShowDialogAsync(_dialogHost, dialogArgs);
                        if (result.Confirmed)
                            filePath = result.File;
                    }
                    else
                    {
                        var dialogArgs = new OpenFileDialogArguments()
                        {
                            Width = 800,
                            Height = 600,
                            Filters = "SQLite files|*.db3;*.db"
                        };
                        var result = await OpenFileDialog.ShowDialogAsync(_dialogHost, dialogArgs);
                        if (result.Confirmed)
                            filePath = result.File;
                    }

                    context.SetOutput(filePath);
                })
                .DisposeWith(disposables);

            this.OneWayBind(ingotsViewModel,
                vm => vm.InUseFile,
                v => v._tbPath.Text)
                .DisposeWith(disposables);
        }

        public IngotsViewModel ViewModel { get; set; }

        public SnackbarMessageQueue MessageQueue { get; } = new SnackbarMessageQueue();

        object IViewFor.ViewModel
        {
            get => ViewModel;
            set => ViewModel = (IngotsViewModel)value;
        }
    }
}