﻿using System.Runtime.Serialization;

namespace IngotsRx.Models
{
    [DataContract]
    public class BankParser
    {
        [DataMember] public string Bank { get; set; }
        [DataMember] public string Separator { get; set; }
        [DataMember] public bool Quotes { get; set; }
        [DataMember] public int Reference { get; set; }
        [DataMember] public int Date { get; set; }
        [DataMember] public int Value { get; set; }
        [DataMember] public int CounterPart { get; set; }
        [DataMember] public int Description { get; set; }
        [DataMember] public int Iban { get; set; }
    }
}