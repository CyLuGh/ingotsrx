﻿using DynamicData;
using IngotsRx.ViewModels;
using System;

namespace IngotsRx
{
    public static class DemoDataUtils
    {
        public static void FillDemo(this IngotsViewModel ivm)
        {
            CreateAccounts(ivm);
            CreateTransactions(ivm);
            CreateTransfers(ivm);
        }

        private static void CreateTransfers(IngotsViewModel ivm)
        {
            ulong transferId = 0;
            TransferViewModel tvm;

            tvm = new TransferViewModel
            {
                Id = ++transferId,
                AccountId = 2,
                TargetId = 3,
                Date = DateTime.Today.AddDays(-200),
                Value = -250,
                Description = $"Transfer {transferId}",
                IsExecuted = true
            };

            ivm.TransfersCache.AddOrUpdate(tvm);

            tvm = new TransferViewModel
            {
                Id = ++transferId,
                AccountId = 2,
                TargetId = 1,
                Date = DateTime.Today.AddDays(-230),
                Value = -200,
                Description = $"Transfer {transferId}",
                IsExecuted = true
            };

            ivm.TransfersCache.AddOrUpdate(tvm);
        }

        private static void CreateTransactions(IngotsViewModel ivm)
        {
            ulong transactionId = 0;
            ivm.TransactionsCache.AddOrUpdate(new TransactionViewModel
            {
                Id = ++transactionId,
                AccountId = 1,
                Date = DateTime.Today.AddDays(-250),
                Value = 300,
                Category = "Test",
                SubCategory = "Demo",
                Description = $"Transaction {transactionId}",
                IsExecuted = true
            });

            ivm.TransactionsCache.AddOrUpdate(new TransactionViewModel
            {
                Id = ++transactionId,
                AccountId = 1,
                Date = DateTime.Today.AddDays(-230),
                Value = -48.94,
                Category = "Test",
                SubCategory = "Demo 2",
                Description = $"Transaction {transactionId}",
                IsExecuted = true
            });

            ivm.TransactionsCache.AddOrUpdate(new TransactionViewModel
            {
                Id = ++transactionId,
                AccountId = 1,
                Date = DateTime.Today.AddDays(-225),
                Value = -24.67,
                Category = "Test",
                SubCategory = "Demo",
                Description = $"Transaction {transactionId}",
                IsExecuted = true
            });

            ivm.TransactionsCache.AddOrUpdate(new TransactionViewModel
            {
                Id = ++transactionId,
                AccountId = 2,
                Date = DateTime.Today.AddDays(30),
                Value = 500,
                Category = "Test",
                SubCategory = "Demo",
                Description = $"Transaction {transactionId}",
                IsExecuted = false
            });

            ivm.TransactionsCache.AddOrUpdate(new TransactionViewModel
            {
                Id = ++transactionId,
                AccountId = 1,
                Date = DateTime.Today.AddDays(-25),
                Value = -0.04,
                Category = "Test",
                SubCategory = "Demo",
                Description = $"Transaction {transactionId}",
                Shop = "Shop 1",
                IsExecuted = true
            });

            ivm.TransactionsCache.AddOrUpdate(new TransactionViewModel
            {
                Id = ++transactionId,
                AccountId = 1,
                Date = DateTime.Today.AddDays(-25),
                Value = -7.01,
                Category = "Test",
                SubCategory = "Demo",
                Description = $"Transaction {transactionId}",
                Shop = "Shop 2",
                IsExecuted = true
            });
        }

        private static void CreateAccounts(IngotsViewModel ivm)
        {
            ivm.AccountsCache.AddOrUpdate(new AccountViewModel
            {
                Id = 1,
                Iban = "BE23812474298391",
                Description = "Demo",
                Bank = "Test"
            });
            ivm.AccountsCache.AddOrUpdate(new AccountViewModel
            {
                Id = 2,
                Iban = "BE09557859732157",
                Description = "Saving demo",
                Kind = Data.AccountKind.Saving,
                Bank = "Test",
                StartValue = 1000
            });
            ivm.AccountsCache.AddOrUpdate(new AccountViewModel
            {
                Id = 3,
                Iban = "FR9017569000407439658771W51",
                Description = "Other country account",
                Bank = "Test 2"
            });
        }
    }
}