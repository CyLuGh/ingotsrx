﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Reactive;
using System.Reactive.Disposables;
using System.Reactive.Linq;
using DynamicData;
using DynamicData.Binding;
using MaterialDesignThemes.Wpf;
using MoreLinq;
using ReactiveUI;

namespace IngotsRx.ViewModels
{
    public class OperationsImportViewModel : ReactiveObject, IActivatableViewModel
    {
        public ViewModelActivator Activator { get; }

        public SourceCache<OperationViewModel, string> OperationsCache { get; }
            = new SourceCache<OperationViewModel, string>(o => o.ImportReference);

        private ReadOnlyObservableCollection<OperationViewModel> _unselectedOperations;
        private ReadOnlyObservableCollection<OperationViewModel> _selectedOperations;
        public ReadOnlyObservableCollection<OperationViewModel> UnselectedOperations => _unselectedOperations;
        public ReadOnlyObservableCollection<OperationViewModel> SelectedOperations => _selectedOperations;

        public ReactiveCommand<Unit, Unit> SelectAllCommand { get; private set; }
        public ReactiveCommand<Unit, Unit> SelectNoneCommand { get; private set; }
        public ReactiveCommand<Unit, IEnumerable<OperationViewModel>> SelectCommand { get; private set; }
        public ReactiveCommand<Unit, IEnumerable<OperationViewModel>> UnselectCommand { get; private set; }

        public Interaction<Unit, IEnumerable<OperationViewModel>> SelectInteraction { get; }
            = new Interaction<Unit, IEnumerable<OperationViewModel>>(RxApp.MainThreadScheduler);

        public Interaction<Unit, IEnumerable<OperationViewModel>> UnselectInteraction { get; }
            = new Interaction<Unit, IEnumerable<OperationViewModel>>(RxApp.MainThreadScheduler);

        public ReactiveCommand<Unit, Unit> ImportCommand { get; private set; }
        public ReactiveCommand<Unit, Unit> CancelCommand { get; private set; }

        public OperationsImportViewModel()
        {
            Activator = new ViewModelActivator();

            InitializeCommands(this);

            this.WhenActivated(disposables =>
            {
                OperationsCache.Connect()
                    .AutoRefresh(x => x.ImportSelection)
                    .Filter(o => (o is TransactionViewModel) || (o is TransferViewModel tvm && tvm.Value < 0))
                    .Filter(o => !o.ImportSelection)
                    .Sort(SortExpressionComparer<OperationViewModel>.Descending(o => o.Date))
                    .ObserveOn(RxApp.MainThreadScheduler)
                    .Bind(out _unselectedOperations)
                    .DisposeMany()
                    .Subscribe()
                    .DisposeWith(disposables);

                OperationsCache.Connect()
                    .AutoRefresh(x => x.ImportSelection)
                    .Filter(o => (o is TransactionViewModel) || (o is TransferViewModel tvm && tvm.Value < 0))
                    .Filter(o => o.ImportSelection)
                    .Sort(SortExpressionComparer<OperationViewModel>.Descending(o => o.Date))
                    .ObserveOn(RxApp.MainThreadScheduler)
                    .Bind(out _selectedOperations)
                    .DisposeMany()
                    .Subscribe()
                    .DisposeWith(disposables);
            });
        }

        private static void InitializeCommands(OperationsImportViewModel @this)
        {
            @this.SelectAllCommand = ReactiveCommand.CreateFromObservable(() =>
                Observable.Start(() => { @this.OperationsCache.Items.ForEach(o => o.ImportSelection = true); }));

            @this.SelectNoneCommand = ReactiveCommand.CreateFromObservable(() =>
                Observable.Start(() => { @this.OperationsCache.Items.ForEach(o => o.ImportSelection = false); }));

            @this.CancelCommand = ReactiveCommand.Create(() =>
            {
                @this.OperationsCache.Items.ForEach(o => o.ImportSelection = false);
                DialogHost.CloseDialogCommand.Execute(false, null);
            });

            @this.ImportCommand = ReactiveCommand.Create(() =>
                DialogHost.CloseDialogCommand.Execute(true, null)
            );

            @this.SelectCommand = ReactiveCommand.CreateFromObservable(() =>
                @this.SelectInteraction.Handle(Unit.Default));
            @this.SelectCommand.SubscribeSafe(operationViewModels =>
                operationViewModels.ForEach(o => o.ImportSelection = true));

            @this.UnselectCommand = ReactiveCommand.CreateFromObservable(() =>
                @this.UnselectInteraction.Handle(Unit.Default));
            @this.UnselectCommand.SubscribeSafe(operationViewModels =>
                operationViewModels.ForEach(o => o.ImportSelection = false));
        }
    }
}