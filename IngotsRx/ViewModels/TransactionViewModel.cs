﻿using IngotsRx.Data;
using ReactiveUI;
using ReactiveUI.Fody.Helpers;
using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Reactive.Disposables;
using System.Reactive.Linq;

namespace IngotsRx.ViewModels
{
    public class TransactionViewModel : OperationViewModel, IEquatable<TransactionViewModel>
    {
        [Reactive] public string Category { get; set; }
        [Reactive] public string SubCategory { get; set; }
        [Reactive] public string Shop { get; set; }

        /// <summary>
        /// Categories and subcategories that have already been used once. Only used/set during edition.
        /// </summary>
        [Reactive] public Dictionary<string, string[]> Categories { get; set; }

        /// <summary>
        /// Shops that have already been used once. Only used/set during edition.
        /// </summary>
        [Reactive] public string[] Shops { get; set; }

        public string[] AvailableCategories { [ObservableAsProperty] get; }
        public string[] AvailableSubcategories { [ObservableAsProperty] get; }
        public string[] AvailableShops { [ObservableAsProperty] get; }

        public TransactionViewModel()
        {
            this.WhenActivated(disposables =>
            {
                this.WhenAnyValue(x => x.Shops)
                    .Where(x => x?.Any() == true)
                    .ObserveOn(RxApp.MainThreadScheduler)
                    .ToPropertyEx(this, x => x.AvailableShops)
                    .DisposeWith(disposables);

                this.WhenAnyValue(x => x.Categories)
                    .WhereNotNull()
                    .Select(d => d.Keys.OrderBy(s => s).ToArray())
                    .ObserveOn(RxApp.MainThreadScheduler)
                    .ToPropertyEx(this, x => x.AvailableCategories)
                    .DisposeWith(disposables);

                this.WhenAnyValue(x => x.Categories)
                    .WhereNotNull()
                    .CombineLatest(this.WhenAnyValue(x => x.Category),
                    (categories, category) => !string.IsNullOrWhiteSpace(category) && categories.TryGetValue(category, out var subs) ? subs.OrderBy(s => s).ToArray() : new string[0])
                    .ObserveOn(RxApp.MainThreadScheduler)
                    .ToPropertyEx(this, x => x.AvailableSubcategories)
                    .DisposeWith(disposables);
            });
        }

        public TransactionViewModel(Transaction transaction) : base(transaction)
        {
            Category = transaction.Category;
            SubCategory = transaction.SubCategory;
            Shop = transaction.Shop;
        }

        public override Operation ToData()
        => new Transaction
        {
            Id = Id,
            AccountId = AccountId,
            Date = Date,
            Value = Value,
            Description = Description,
            IsExecuted = IsExecuted,
            Category = Category,
            SubCategory = SubCategory,
            Shop = Shop
        };

        public override object Clone() // Don't use MemberWiseClone as it messes with DynamicData
            => new TransactionViewModel
            {
                Id = Id,
                AccountId = AccountId,
                Date = Date,
                Value = Value,
                Description = Description,
                IsExecuted = IsExecuted,
                Category = Category,
                SubCategory = SubCategory,
                Shop = Shop
            };

        public bool Equals([AllowNull] TransactionViewModel other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return base.Equals(other);
        }

        public override bool Equals(object obj)
            => Equals(obj as TransactionViewModel);

        public override int GetHashCode()
            => base.GetHashCode();
    }
}