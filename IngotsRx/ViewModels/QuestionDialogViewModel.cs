﻿using MaterialDesignThemes.Wpf;
using ReactiveUI;
using ReactiveUI.Fody.Helpers;
using System.Reactive;

namespace IngotsRx.ViewModels
{
    public enum QuestionAnswer { None, Accept, Cancel }

    public class QuestionDialogViewModel : ReactiveObject
    {
        [Reactive] public string Title { get; set; }
        [Reactive] public string Message { get; set; }
        [Reactive] public string AcceptText { get; set; }
        [Reactive] public string CancelText { get; set; }

        [Reactive] public QuestionAnswer Answer { get; set; }

        public ReactiveCommand<Unit, Unit> AcceptCommand { get; }
        public ReactiveCommand<Unit, Unit> CancelCommand { get; }

        public QuestionDialogViewModel()
        {
            AcceptCommand = ReactiveCommand.Create(() =>
            {
                Answer = QuestionAnswer.Accept;
                DialogHost.CloseDialogCommand.Execute(true, null);
            });

            CancelCommand = ReactiveCommand.Create(() =>
            {
                Answer = QuestionAnswer.Cancel;
                DialogHost.CloseDialogCommand.Execute(false, null);
            });
        }
    }
}