﻿using IngotsRx.Data;
using LanguageExt;
using ReactiveUI;
using ReactiveUI.Fody.Helpers;
using System;
using System.Collections;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.Reactive;
using System.Reactive.Disposables;
using System.Reactive.Linq;

namespace IngotsRx.ViewModels
{
    public abstract class OperationViewModel : ReactiveObject, IActivatableViewModel, ICloneable, IEquatable<OperationViewModel>
    {
        [Reactive] public ulong Id { get; set; }
        [Reactive] public long AccountId { get; set; }
        [Reactive] public DateTime Date { get; set; } = DateTime.Today;
        [Reactive] public double Value { get; set; } = 0d;
        [Reactive] public string ValueInput { get; set; }
        [Reactive] public string Description { get; set; }
        [Reactive] public bool IsDerived { get; set; }
        [Reactive] public bool IsExecuted { get; set; }
        [Reactive] public string ErrorMessage { get; set; }
        [Reactive] public string ImportReference { get; set; }
        [Reactive] public bool ImportSelection { get; set; }

        public ReactiveCommand<System.Reactive.Unit, System.Reactive.Unit> ValidateCommand { get; private set; }

        public Interaction<System.Reactive.Unit, System.Reactive.Unit> ValidateInteraction { get; }
            = new Interaction<System.Reactive.Unit, System.Reactive.Unit>(RxApp.MainThreadScheduler);

        protected OperationViewModel()
        {
            Activator = new ViewModelActivator();

            ValidateInteraction.RegisterHandler(_ => _.SetOutput(System.Reactive.Unit.Default));
            InitializeCommands(this);

            this.WhenActivated(disposables =>
            {
                this.WhenAnyValue(x => x.Date)
                    .Select(x => x <= DateTime.Today)
                    .SubscribeSafe(x => IsExecuted = x)
                    .DisposeWith(disposables);

                this.WhenAnyValue(x => x.ValueInput)
                    .Throttle(TimeSpan.FromMilliseconds(150))
                    .Select(s =>
                    {
                        if (string.IsNullOrWhiteSpace(s))
                            return Option<double>.Some(0d);

                        if (s.Contains(NumberFormatInfo.CurrentInfo.NumberGroupSeparator)
                          && !s.Contains(NumberFormatInfo.CurrentInfo.NumberDecimalSeparator))
                            return Option<double>.None;

                        return double.TryParse(s, out var d) ? Option<double>.Some(d) : Option<double>.None;
                    })
                    .ObserveOn(RxApp.MainThreadScheduler)
                    .SubscribeSafe(o => o.Match(
                        d =>
                        {
                            ErrorMessage = string.Empty;
                            Value = d;
                        },
                        () =>
                        {
                            ErrorMessage = "Invalid amount input";
                            Value = 0d;
                        }))
                    .DisposeWith(disposables);

                ValueInput = Value.ToString("F2");
            });
        }

        private static void InitializeCommands(OperationViewModel @this)
        {
            @this.ValidateCommand
                = ReactiveCommand.CreateFromObservable(() => @this.ValidateInteraction.Handle(System.Reactive.Unit.Default),
                @this.WhenAnyValue(x => x.ErrorMessage).Select(s => string.IsNullOrEmpty(s)).ObserveOn(RxApp.MainThreadScheduler));
        }

        protected OperationViewModel(Operation operation) : this()
        {
            Id = operation.Id;
            AccountId = operation.AccountId;
            Date = operation.Date;
            Value = operation.Value;
            Description = operation.Description;
            IsExecuted = operation.IsExecuted;
        }

        #region Interfaces

        public ViewModelActivator Activator { get; }

        #endregion Interfaces

        public abstract Operation ToData();

        public abstract object Clone();

        public bool Equals([AllowNull] OperationViewModel other)
        {
            if (ReferenceEquals(null, other))
                return false;
            if (ReferenceEquals(this, other))
                return true;
            return Id == other.Id && AccountId == other.AccountId;
        }

        public override bool Equals(object obj)
            => Equals(obj as OperationViewModel);

        public override int GetHashCode()
        {
            unchecked
            {
                return (Id.GetHashCode() * 397) ^ AccountId.GetHashCode();
            }
        }

        public IEnumerable GetErrors(string propertyName)
        {
            throw new NotImplementedException();
        }
    }
}