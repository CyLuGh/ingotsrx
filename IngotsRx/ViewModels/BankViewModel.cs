﻿using DynamicData;
using DynamicData.Binding;
using ReactiveUI;
using ReactiveUI.Fody.Helpers;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Reactive.Disposables;
using System.Reactive.Linq;
using System.Text;

namespace IngotsRx.ViewModels
{
    public class BankViewModel : ReactiveObject, IActivatableViewModel
    {
        [Reactive] public string Name { get; set; }
        [Reactive] public AccountViewModel SelectedAccount { get; set; }

        public IObservableCache<AccountViewModel, long> AccountsCache { get; }

        private ReadOnlyObservableCollection<AccountViewModel> _accounts;
        public ReadOnlyObservableCollection<AccountViewModel> Accounts => _accounts;

        public double TotalValue { [ObservableAsProperty] get; }

        public BankViewModel(AccountsViewModel accountsViewModel, IObservableCache<AccountViewModel, long> accountsCache)
        {
            Activator = new ViewModelActivator();
            AccountsCache = accountsCache;

            this.WhenActivated(disposables =>
            {
                AccountsCache
                    .Connect()
                    .Sort(SortExpressionComparer<AccountViewModel>.Ascending(a => a.Kind)
                                                                  .ThenByAscending(a => a.Description))
                    .Bind(out _accounts)
                    .DisposeMany()
                    .Subscribe()
                    .DisposeWith(disposables);

                this.WhenAnyValue(x => x.SelectedAccount)
                    .WhereNotNull()
                    .ObserveOn(RxApp.MainThreadScheduler)
                    .SubscribeSafe(sel => accountsViewModel.SelectedAccount = sel)
                    .DisposeWith(disposables);

                accountsViewModel.WhenAnyValue(x => x.SelectedAccount)
                                 .ObserveOn(RxApp.MainThreadScheduler)
                                 .SubscribeSafe(sel =>
                                 {
                                     if (Accounts.Contains(sel))
                                         SelectedAccount = sel;
                                     else
                                         SelectedAccount = null;
                                 })
                                 .DisposeWith(disposables);

                AccountsCache.Connect()
                             .AutoRefresh(x => x.StartValue)
                             .AutoRefresh(x => x.OperationsValue)
                             .Select(_ => accountsCache.Items.Sum(o => o.StartValue + o.OperationsValue))
                             .ObserveOn(RxApp.MainThreadScheduler)
                             .ToPropertyEx(this, x => x.TotalValue)
                             .DisposeWith(disposables);
            });
        }

        public ViewModelActivator Activator { get; }
    }
}