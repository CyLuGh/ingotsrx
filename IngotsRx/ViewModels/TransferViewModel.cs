﻿using IngotsRx.Data;
using ReactiveUI.Fody.Helpers;
using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Text;

namespace IngotsRx.ViewModels
{
    public class TransferViewModel : OperationViewModel, IEquatable<TransferViewModel>
    {
        [Reactive] public long TargetId { get; set; }

        /// <summary>
        /// Array of possible target account ids. Only used/set during edition.
        /// </summary>
        [Reactive] public AccountViewModel[] AvailableTargets { get; set; }

        public TransferViewModel()
        {
        }

        public TransferViewModel(Transfer transfer) : base(transfer)
        {
            TargetId = transfer.TargetId;
            IsDerived = transfer.IsDerived;
        }

        internal TransferViewModel OppositeTransfer()
        {
            var opposite = (TransferViewModel)Clone();
            opposite.Value = Value * -1;
            opposite.AccountId = TargetId;
            opposite.TargetId = AccountId;
            opposite.IsDerived = true;
            return opposite;
        }

        public override Operation ToData()
            => new Transfer
            {
                Id = Id,
                AccountId = AccountId,
                Date = Date,
                Value = Value,
                Description = Description,
                IsExecuted = IsExecuted,
                TargetId = TargetId,
                IsDerived = IsDerived
            };

        public override object Clone() // Don't use MemberWiseClone as it messes with DynamicData
             => new TransferViewModel
             {
                 Id = Id,
                 AccountId = AccountId,
                 Date = Date,
                 Value = Value,
                 Description = Description,
                 IsExecuted = IsExecuted,
                 TargetId = TargetId,
                 IsDerived = IsDerived
             };

        public bool Equals([AllowNull] TransferViewModel other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return base.Equals(other) && TargetId == other.TargetId && IsDerived == other.IsDerived;
        }

        public override bool Equals(object obj)
            => Equals(obj as TransferViewModel);

        public override int GetHashCode()
        {
            unchecked
            {
                int hashCode = base.GetHashCode();
                hashCode = (hashCode * 397) ^ TargetId.GetHashCode();
                hashCode = (hashCode * 397) ^ IsDerived.GetHashCode();
                return hashCode;
            }
        }
    }
}