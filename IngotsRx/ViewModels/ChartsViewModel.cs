﻿using DynamicData;
using IngotsRx.Views.Infrastructure;
using MoreLinq;
using ReactiveUI;
using Splat;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reactive;
using System.Reactive.Disposables;
using System.Reactive.Linq;
using System.Text;

namespace IngotsRx.ViewModels
{
    public class ChartsViewModel : ReactiveObject, IActivatableViewModel, IRoutableViewModel
    {
        public ReactiveCommand<IChangeSet<(DateTime, double), DateTime>, ChartSeries> BuildYearlyIncomeSeriesCommand { get; private set; }
        public ReactiveCommand<IChangeSet<(DateTime, double), DateTime>, ChartSeries> BuildYearlyExpenseSeriesCommand { get; private set; }
        public ReactiveCommand<IChangeSet<(DateTime, double), DateTime>, ChartSeries> BuildMonthlyIncomeSeriesCommand { get; private set; }
        public ReactiveCommand<IChangeSet<(DateTime, double), DateTime>, ChartSeries> BuildMonthlyExpenseSeriesCommand { get; private set; }

        public Interaction<ChartSeries, Unit> AddMonthlyIncomeSeriesInteraction { get; }
            = new Interaction<ChartSeries, Unit>(RxApp.MainThreadScheduler);

        public Interaction<ChartSeries, Unit> AddMonthlyExpenseSeriesInteraction { get; }
            = new Interaction<ChartSeries, Unit>(RxApp.MainThreadScheduler);

        public Interaction<ChartSeries, Unit> AddYearlyIncomeSeriesInteraction { get; }
           = new Interaction<ChartSeries, Unit>(RxApp.MainThreadScheduler);

        public Interaction<ChartSeries, Unit> AddYearlyExpenseSeriesInteraction { get; }
            = new Interaction<ChartSeries, Unit>(RxApp.MainThreadScheduler);

        public ChartsViewModel(IngotsViewModel ingotsViewModel)
        {
            HostScreen = ingotsViewModel ?? Locator.Current.GetService<IScreen>();
            Activator = new ViewModelActivator();
            IngotsViewModel = ingotsViewModel;

            InitializeCommands(this);

            this.WhenActivated(disposables =>
            {
                ingotsViewModel.TransactionsCache
                    .Connect()
                    .AutoRefresh(o => o.IsExecuted)
                    .Filter(o => o.Value < 0)
                    .Group(t => new DateTime(t.Date.Year, t.Date.Month, 1))
                    .Transform(g => (Period: g.Key, Value: Math.Abs(g.Cache.Items.Sum(x => x.Value))))
                    .InvokeCommand(BuildMonthlyExpenseSeriesCommand)
                    .DisposeWith(disposables);

                ingotsViewModel.TransactionsCache
                    .Connect()
                    .AutoRefresh(o => o.IsExecuted)
                    .Filter(o => o.Value > 0)
                    .Group(t => new DateTime(t.Date.Year, t.Date.Month, 1))
                    .Transform(g => (Period: g.Key, Value: Math.Abs(g.Cache.Items.Sum(x => x.Value))))
                    .InvokeCommand(BuildMonthlyIncomeSeriesCommand)
                    .DisposeWith(disposables);

                ingotsViewModel.TransactionsCache
                   .Connect()
                   .AutoRefresh(o => o.IsExecuted)
                   .Filter(o => o.Value < 0)
                   .Group(t => new DateTime(t.Date.Year, 1, 1))
                   .Transform(g => (Period: g.Key, Value: Math.Abs(g.Cache.Items.Sum(x => x.Value))))
                   .InvokeCommand(BuildYearlyExpenseSeriesCommand)
                   .DisposeWith(disposables);

                ingotsViewModel.TransactionsCache
                    .Connect()
                    .AutoRefresh(o => o.IsExecuted)
                    .Filter(o => o.Value > 0)
                    .Group(t => new DateTime(t.Date.Year, 1, 1))
                    .Transform(g => (Period: g.Key, Value: Math.Abs(g.Cache.Items.Sum(x => x.Value))))
                    .InvokeCommand(BuildYearlyIncomeSeriesCommand)
                    .DisposeWith(disposables);
            });
        }

        private static void InitializeCommands(ChartsViewModel @this)
        {
            @this.BuildYearlyIncomeSeriesCommand = ReactiveCommand.CreateFromObservable<IChangeSet<(DateTime Period, double Value), DateTime>, ChartSeries>(changes => Observable.Start(() =>
            {
                var data = changes.Where(x => x.Reason == ChangeReason.Add).Select(x => x.Current)
                  .ToDictionary(o => o.Period, o => o.Value);
                return new ChartSeries
                {
                    Title = "Income",
                    DataSource = data
                };
            }));
            @this.BuildYearlyIncomeSeriesCommand.SubscribeSafe(async cs => await @this.AddYearlyIncomeSeriesInteraction.Handle(cs));
            @this.BuildYearlyIncomeSeriesCommand
                .ThrownExceptions
                .InvokeCommand(@this.IngotsViewModel.LoggingViewModel, x => x.LogExceptionCommand);

            @this.BuildYearlyExpenseSeriesCommand = ReactiveCommand.CreateFromObservable<IChangeSet<(DateTime Period, double Value), DateTime>, ChartSeries>(changes => Observable.Start(() =>
            {
                var data = changes.Where(x => x.Reason == ChangeReason.Add).Select(x => x.Current)
                   .ToDictionary(o => o.Period, o => o.Value);
                return new ChartSeries
                {
                    Title = "Expenses",
                    DataSource = data
                };
            }));
            @this.BuildYearlyExpenseSeriesCommand.SubscribeSafe(async cs => await @this.AddYearlyExpenseSeriesInteraction.Handle(cs));
            @this.BuildYearlyExpenseSeriesCommand
                .ThrownExceptions
                .InvokeCommand(@this.IngotsViewModel.LoggingViewModel, x => x.LogExceptionCommand);

            @this.BuildMonthlyIncomeSeriesCommand = ReactiveCommand.CreateFromObservable<IChangeSet<(DateTime Period, double Value), DateTime>, ChartSeries>(changes => Observable.Start(() =>
            {
                var data = changes.Where(x => x.Reason == ChangeReason.Add).Select(x => x.Current)
                   .ToDictionary(o => o.Period, o => o.Value);
                return new ChartSeries
                {
                    Title = "Income",
                    DataSource = data
                };
            }));
            @this.BuildMonthlyIncomeSeriesCommand.SubscribeSafe(async cs => await @this.AddMonthlyIncomeSeriesInteraction.Handle(cs));
            @this.BuildMonthlyIncomeSeriesCommand
                .ThrownExceptions
                .InvokeCommand(@this.IngotsViewModel.LoggingViewModel, x => x.LogExceptionCommand);

            @this.BuildMonthlyExpenseSeriesCommand = ReactiveCommand.CreateFromObservable<IChangeSet<(DateTime Period, double Value), DateTime>, ChartSeries>(changes => Observable.Start(() =>
            {
                var data = changes.Where(x => x.Reason == ChangeReason.Add).Select(x => x.Current)
                   .ToDictionary(o => o.Period, o => o.Value);
                return new ChartSeries
                {
                    Title = "Expenses",
                    DataSource = data
                };
            }));
            @this.BuildMonthlyExpenseSeriesCommand.SubscribeSafe(async cs => await @this.AddMonthlyExpenseSeriesInteraction.Handle(cs));
            @this.BuildMonthlyExpenseSeriesCommand
                .ThrownExceptions
                .InvokeCommand(@this.IngotsViewModel.LoggingViewModel, x => x.LogExceptionCommand);
        }

        public ViewModelActivator Activator { get; }

        public string UrlPathSegment => "Charts";
        public IScreen HostScreen { get; }
        public IngotsViewModel IngotsViewModel { get; }
    }
}