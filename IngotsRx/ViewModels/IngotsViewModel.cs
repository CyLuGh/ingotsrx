﻿using DynamicData;
using IngotsRx.Data;
using IngotsRx.Views.Converters;
using MaterialDesignExtensions.Model;
using MaterialDesignThemes.Wpf;
using MoreLinq;
using NLog;
using ReactiveUI;
using ReactiveUI.Fody.Helpers;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Reactive;
using System.Reactive.Disposables;
using System.Reactive.Linq;
using System.Reflection;

namespace IngotsRx.ViewModels
{
    public class IngotsViewModel : ReactiveObject, IActivatableViewModel, IScreen
    {
        private static readonly NLog.ILogger _logger = LogManager.GetCurrentClassLogger();

        internal SourceCache<AccountViewModel, long> AccountsCache { get; } = new SourceCache<AccountViewModel, long>(a => a.Id);
        internal SourceCache<TransactionViewModel, (ulong, bool, Type)> TransactionsCache { get; } = new SourceCache<TransactionViewModel, (ulong, bool, Type)>(t => (t.Id, t.IsDerived, typeof(TransactionViewModel)));
        internal SourceCache<TransferViewModel, (ulong, bool, Type)> TransfersCache { get; } = new SourceCache<TransferViewModel, (ulong, bool, Type)>(t => (t.Id, t.IsDerived, typeof(TransferViewModel)));

        public AccountsViewModel AccountsViewModel { get; }
        public ChartsViewModel ChartsViewModel { get; }
        public BalancesViewModel BalancesViewModel { get; }
        public LoggingViewModel LoggingViewModel { get; }

        public ReactiveCommand<string, Unit> ShowMessageCommand { get; private set; }

        public Interaction<string, Unit> ShowMessageInteraction { get; }
            = new Interaction<string, Unit>(RxApp.MainThreadScheduler);

        public ReactiveCommand<string, Unit> CreateDatabaseCommand { get; private set; }
        public ReactiveCommand<string, Unit> LoadDatabaseCommand { get; private set; }
        public ReactiveCommand<bool, (bool isCreate, string filePath)> PickDatabaseCommand { get; private set; }

        public Interaction<bool, string> PickDatabaseInteraction { get; }
            = new Interaction<bool, string>(RxApp.MainThreadScheduler);

        [Reactive] public string FilePath { get; set; }
        public string InUseFile { [ObservableAsProperty] get; }

        public List<INavigationItem> NavigationItems => new List<INavigationItem> {
            new FirstLevelNavigationItem {Label = "Accounts", Icon = PackIconKind.Bank},
            new FirstLevelNavigationItem {Label = "Charts", Icon = PackIconKind.ChartBar},
            new FirstLevelNavigationItem {Label = "Balances", Icon = PackIconKind.ScaleBalance}
        };

        [Reactive] public INavigationItem SelectedNavigationItem { get; set; }

        public IngotsViewModel()
        {
            Router = new RoutingState();
            Activator = new ViewModelActivator();

            LoggingViewModel = new LoggingViewModel(this);

            AccountsViewModel = new AccountsViewModel(this);
            ChartsViewModel = new ChartsViewModel(this);
            BalancesViewModel = new BalancesViewModel(this);

            InitializeCommands(this);

            SetAccountsUpdaters(TransactionsCache, t => t.AccountId);
            SetAccountsUpdaters(TransfersCache, t => t.AccountId);
            SetAccountsUpdaters(TransfersCache, t => t.TargetId, multiplier: -1);

            this.WhenActivated(disposables =>
            {
                AccountsCache.Connect()
                    .SubscribeSafe(changes =>
                    {
                        changes.Where(x => x.Reason == ChangeReason.Add).Select(x => x.Current)
                            .ForEach(change => IdAccountConverter.Instance.Accounts.Add(change.Id, change));

                        changes.Where(x => x.Reason == ChangeReason.Remove).Select(x => x.Current)
                            .ForEach(change => IdAccountConverter.Instance.Accounts.Remove(change.Id));
                    })
                    .DisposeWith(disposables);

                Observable.Return((EventKind.Info, "Welcome to IngotsRx!"))
                    .Delay(TimeSpan.FromMilliseconds(100))
                    .InvokeCommand(LoggingViewModel, x => x.LogMessageCommand);

                this.WhenAnyValue(x => x.FilePath)
                    .Do(filePath =>
                    {
                        AccountsViewModel.SelectedAccount = null;
                        AccountsViewModel.SelectedOperation = null;
                        if (!string.IsNullOrWhiteSpace(filePath))
                            SaveSettings();
                    })
                    .ObserveOn(RxApp.MainThreadScheduler)
                    .ToPropertyEx(this, x => x.InUseFile)
                    .DisposeWith(disposables);

                Observable.Return(Unit.Default)
                    .Delay(TimeSpan.FromMilliseconds(100))
                    .SubscribeSafe(_ => ReloadSettings());

                this.WhenAnyValue(o => o.SelectedNavigationItem).Select(s => s as FirstLevelNavigationItem)
                    .WhereNotNull()
                    .DistinctUntilChanged()
                    .SubscribeSafe(n =>
                    {
                        Observable.Return(GetViewModel(n))
                                 .WhereNotNull()
                                 .InvokeCommand(Router, x => x.Navigate);
                    }).DisposeWith(disposables);

                AccountsViewModel.Activator.Activated.SubscribeSafe(_ => _logger.Debug("AccountsViewModel is active")).DisposeWith(disposables);
                AccountsViewModel.Activator.Deactivated.SubscribeSafe(_ => _logger.Debug("AccountsViewModel is unactive")).DisposeWith(disposables);
                ChartsViewModel.Activator.Activated.SubscribeSafe(_ => _logger.Debug("ChartsViewModel is active")).DisposeWith(disposables);
                ChartsViewModel.Activator.Deactivated.SubscribeSafe(_ => _logger.Debug("ChartsViewModel is unactive")).DisposeWith(disposables);
                BalancesViewModel.Activator.Activated.SubscribeSafe(_ => _logger.Debug("BalancesViewModel is active")).DisposeWith(disposables);
                BalancesViewModel.Activator.Deactivated.SubscribeSafe(_ => _logger.Debug("BalancesViewModel is unactive")).DisposeWith(disposables);
            });
        }

        private void SetAccountsUpdaters<T>(SourceCache<T, (ulong, bool, Type)> cache, Func<T, long> selector, int multiplier = 1) where T : OperationViewModel
        {
            cache.Connect()
                .AutoRefresh(x => x.IsExecuted)
                .SubscribeSafe(changes =>
                {
                    changes.Where(x => x.Reason == ChangeReason.Add).Select(x => x.Current).ForEach(change =>
                    {
                        var account = AccountsCache.Items.FirstOrDefault(o => o.Id == selector(change));
                        if (account != null && change.IsExecuted)
                            account.OperationsValue += change.Value * multiplier;
                    });

                    changes.Where(x => x.Reason == ChangeReason.Remove).Select(x => x.Current).ForEach(change =>
                    {
                        var account = AccountsCache.Items.FirstOrDefault(o => o.Id == selector(change));
                        if (account != null && change.IsExecuted)
                            account.OperationsValue -= change.Value * multiplier;
                    });

                    changes.Where(x => x.Reason == ChangeReason.Update).ForEach(change =>
                    {
                        var account = AccountsCache.Items.FirstOrDefault(o => o.Id == selector(change.Previous.Value));
                        if (account != null && change.Previous.Value.IsExecuted)
                            account.OperationsValue -= change.Previous.Value.Value * multiplier;
                        account = AccountsCache.Items.FirstOrDefault(o => o.Id == selector(change.Current));
                        if (account != null && change.Current.IsExecuted)
                            account.OperationsValue += change.Current.Value * multiplier;
                    });

                    changes.Where(x => x.Reason == ChangeReason.Refresh).Select(x => x.Current).ForEach(change =>
                    {
                        var account = AccountsCache.Items.FirstOrDefault(o => o.Id == selector(change));
                        if (account != null)
                            account.OperationsValue += change.Value * (change.IsExecuted ? 1 : -1) * multiplier;
                    });
                });
        }

        private void SaveSettings()
        {
            Configuration config = ConfigurationManager.OpenExeConfiguration(Assembly.GetEntryAssembly().Location);

            if (config.AppSettings.Settings["dbPath"] == null)
                config.AppSettings.Settings.Add("dbPath", FilePath);
            else
                config.AppSettings.Settings["dbPath"].Value = FilePath;

            config.Save(ConfigurationSaveMode.Modified, true);
        }

        private void ReloadSettings()
        {
            Configuration config = ConfigurationManager.OpenExeConfiguration(Assembly.GetEntryAssembly().Location);
            var dbPath = config.AppSettings.Settings["dbPath"];

            if (dbPath != null && !string.IsNullOrWhiteSpace(dbPath.Value))
                Observable.Return(dbPath.Value).InvokeCommand(LoadDatabaseCommand);
        }

        private IRoutableViewModel GetViewModel(int idx)
        {
            return idx switch
            {
                1 => AccountsViewModel,
                2 => ChartsViewModel,
                3 => BalancesViewModel,
                _ => null
            };
        }

        private IRoutableViewModel GetViewModel(FirstLevelNavigationItem item)
        {
            return item.Label switch
            {
                "Accounts" => AccountsViewModel,
                "Charts" => ChartsViewModel,
                "Balances" => BalancesViewModel,
                _ => null
            };
        }

        private static void InitializeCommands(IngotsViewModel @this)
        {
            @this.CreateDatabaseCommand = ReactiveCommand.CreateFromObservable<string, Unit>(filePath =>
                Observable.Start(() =>
                {
                    @this.Clear();

                    string[] extensions = { ".db3", ".db" };
                    if (!extensions.Contains(Path.GetExtension(filePath).ToLower()))
                        filePath = $"{filePath}.db3";

                    DataManager.Instance.CreateDatabase(filePath);
                    DataManager.Instance.Path = filePath;

                    @this.FilePath = filePath;
                    Observable.Return((EventKind.Info, $"Created database: {filePath}")).InvokeCommand(@this.LoggingViewModel, x => x.LogMessageCommand);
                }));
            @this.CreateDatabaseCommand.ThrownExceptions
                .InvokeCommand(@this.LoggingViewModel, x => x.LogExceptionCommand);

            @this.LoadDatabaseCommand = ReactiveCommand.CreateFromObservable<string, Unit>(filePath =>
                Observable.Start(() =>
                {
                    @this.Clear();

                    DataManager.Instance.Path = filePath;
                    var accounts = DataManager.Instance.GetAccounts().Select(o => new AccountViewModel(o));
                    var transactions = DataManager.Instance.GetTransactions().Select(o => new TransactionViewModel(o));
                    var transfers = DataManager.Instance.GetTransfers().Select(o => new TransferViewModel(o));

                    @this.AccountsCache.AddOrUpdate(accounts);
                    @this.TransactionsCache.AddOrUpdate(transactions);
                    @this.TransfersCache.AddOrUpdate(transfers);

                    @this.FilePath = filePath;
                    Observable.Return((EventKind.Info, $"Loaded database: {filePath}")).InvokeCommand(@this.LoggingViewModel, x => x.LogMessageCommand);
                }));
            @this.LoadDatabaseCommand.ThrownExceptions
                .InvokeCommand(@this.LoggingViewModel, x => x.LogExceptionCommand);

            @this.PickDatabaseCommand = ReactiveCommand.CreateFromObservable<bool, (bool, string)>(isCreate
                => @this.PickDatabaseInteraction.Handle(isCreate).Select(s => (isCreate, s)));
            @this.PickDatabaseCommand.ThrownExceptions
                .InvokeCommand(@this.LoggingViewModel, x => x.LogExceptionCommand);
            @this.PickDatabaseCommand.Where(t => !string.IsNullOrWhiteSpace(t.filePath) && t.isCreate)
                .Select(t => t.filePath)
                .InvokeCommand(@this, x => x.CreateDatabaseCommand);
            @this.PickDatabaseCommand.Where(t => !string.IsNullOrWhiteSpace(t.filePath) && !t.isCreate)
                .Select(t => t.filePath)
                .InvokeCommand(@this, x => x.LoadDatabaseCommand);
        }

        private void Clear()
        {
            IdAccountConverter.Instance.Accounts.Clear();
            AccountsCache.Clear();
            TransactionsCache.Clear();
            TransfersCache.Clear();
        }

        public ViewModelActivator Activator { get; }
        public RoutingState Router { get; }
    }
}