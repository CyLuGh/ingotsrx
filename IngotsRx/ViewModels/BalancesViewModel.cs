﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Reactive.Disposables;
using System.Reactive.Linq;
using DynamicData;
using DynamicData.Binding;
using ReactiveUI;
using ReactiveUI.Fody.Helpers;
using Splat;

namespace IngotsRx.ViewModels
{
    public class BalancesViewModel : ReactiveObject, IActivatableViewModel, IRoutableViewModel
    {
        public IngotsViewModel IngotsViewModel { get; }
        public ViewModelActivator Activator { get; }
        public int AccountsCount { [ObservableAsProperty] get; }
        public double TotalAvailable { [ObservableAsProperty] get; }

        private ReadOnlyObservableCollection<StashViewModel> _stashes;
        public ReadOnlyObservableCollection<StashViewModel> Stashes => _stashes;

        private ReadOnlyObservableCollection<BalanceViewModel> _balances;
        public ReadOnlyObservableCollection<BalanceViewModel> Balances => _balances;

        public BalancesViewModel(IngotsViewModel ingotsViewModel)
        {
            HostScreen = ingotsViewModel ?? Locator.Current.GetService<IScreen>();
            Activator = new ViewModelActivator();
            IngotsViewModel = ingotsViewModel;

            this.WhenActivated(disposables =>
            {
                IngotsViewModel.AccountsCache
                               .Connect()
                               .Select(_ => IngotsViewModel.AccountsCache.Count)
                               .ObserveOn(RxApp.MainThreadScheduler)
                               .ToPropertyEx(this, x => x.AccountsCount)
                               .DisposeWith(disposables);

                IngotsViewModel.AccountsCache
                               .Connect()
                               .AutoRefresh(x => x.StartValue)
                               .AutoRefresh(x => x.OperationsValue)
                               .Select(_ => IngotsViewModel.AccountsCache.Items.Sum(o => o.StartValue + o.OperationsValue))
                               .ObserveOn(RxApp.MainThreadScheduler)
                               .ToPropertyEx(this, x => x.TotalAvailable)
                               .DisposeWith(disposables);

                IngotsViewModel.AccountsCache
                               .Connect()
                               .Group(avm => avm.Stash)
                               .Transform(g =>
                               {
                                   var svm = new StashViewModel {Name = g.Key, ComponentsCache = g.Cache};
                                   svm.Activator.Activate();
                                   return svm;
                               })
                               .ObserveOn(RxApp.MainThreadScheduler)
                               .Bind(out _stashes)
                               .DisposeMany()
                               .Subscribe()
                               .DisposeWith(disposables);

                IngotsViewModel.TransactionsCache
                               .Connect()
                               .ObserveOn(RxApp.TaskpoolScheduler)
                               .AutoRefresh(x => x.Value)
                               .AutoRefresh(x => x.IsExecuted)
                               .Group(tvm => new DateTime(tvm.Date.Year, tvm.Date.Month, 1))
                               .Transform(g => new BalanceViewModel
                                               {
                                                   Period = g.Key,
                                                   Income = g.Cache.Items.Where(o => o.Value > 0).Sum(o => o.Value),
                                                   Expense = g.Cache.Items.Where(o => o.Value < 0).Sum(o => Math.Abs(o.Value))
                                               })
                               .Group(bvm => new DateTime( bvm.Period.Year, 1, 1))
                               .Transform(g => new BalanceViewModel
                                               {
                                                   Period = g.Key,
                                                   ComponentsCache = g.Cache
                                               })
                               .Sort( SortExpressionComparer<BalanceViewModel>.Descending(x=>x.Period))
                               .ObserveOn(RxApp.MainThreadScheduler)
                               .Bind(out _balances)
                               .Subscribe()
                               .DisposeWith(disposables);
            });
        }

        public string UrlPathSegment => "Balances";
        public IScreen HostScreen { get; }
    }
}