﻿using DynamicData;
using DynamicData.Binding;
using IngotsRx.Data;
using IngotsRx.Views.Infrastructure;
using LanguageExt;
using NLog;
using ReactiveUI;
using ReactiveUI.Fody.Helpers;
using Splat;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Reactive.Disposables;
using System.Reactive.Linq;
using System.Reflection;
using System.Threading.Tasks;
using IngotsRx.Models;
using Newtonsoft.Json;

namespace IngotsRx.ViewModels
{
    public class AccountsViewModel : ReactiveObject, IActivatableViewModel, IRoutableViewModel
    {
        private static readonly NLog.ILogger _logger = LogManager.GetCurrentClassLogger();

        public IngotsViewModel IngotsViewModel { get; }

        private ChartSeries _chartSeries;

        private ReadOnlyObservableCollection<BankViewModel> _banks;
        public ReadOnlyObservableCollection<BankViewModel> Banks => _banks;

        private ReadOnlyObservableCollection<OperationViewModel> _operations;

        public ReadOnlyObservableCollection<OperationViewModel> Operations => _operations;

        [Reactive] public AccountViewModel SelectedAccount { get; set; }

        [Reactive] public OperationViewModel SelectedOperation { get; set; }

        [Reactive] public int DrawnMonths { get; set; } = 24;

        [Reactive] public string SearchText { get; set; }

        public ReactiveCommand<System.Reactive.Unit, Option<AccountViewModel>> AddAccountCommand { get; private set; }

        public ReactiveCommand<AccountViewModel, Option<AccountViewModel>> EditAccountCommand { get; private set; }

        public Interaction<AccountViewModel, Option<AccountViewModel>> EditAccountInteraction { get; }
            = new Interaction<AccountViewModel, Option<AccountViewModel>>(RxApp.MainThreadScheduler);

        public ReactiveCommand<AccountViewModel, AccountViewModel> SaveAccountCommand { get; private set; }

        public ReactiveCommand<System.Reactive.Unit, Option<TransactionViewModel>> AddTransactionCommand { get; private set; }

        public Interaction<TransactionViewModel, Option<TransactionViewModel>> EditTransactionInteraction { get; }
            = new Interaction<TransactionViewModel, Option<TransactionViewModel>>(RxApp.MainThreadScheduler);

        public ReactiveCommand<IEnumerable<TransactionViewModel>, IEnumerable<TransactionViewModel>> SaveTransactionsCommand { get; private set; }
        public ReactiveCommand<IEnumerable<TransferViewModel>, IEnumerable<TransferViewModel>> SaveTransfersCommand { get; private set; }

        public ReactiveCommand<TransactionViewModel, TransactionViewModel> SaveTransactionCommand { get; private set; }

        public ReactiveCommand<System.Reactive.Unit, Option<TransferViewModel>> AddTransferCommand { get; private set; }

        public Interaction<TransferViewModel, Option<TransferViewModel>> EditTransferInteraction { get; }
            = new Interaction<TransferViewModel, Option<TransferViewModel>>(RxApp.MainThreadScheduler);

        public ReactiveCommand<TransferViewModel, TransferViewModel> SaveTransferCommand { get; private set; }

        public ReactiveCommand<OperationViewModel[], ChartSeries> BuildChartSeriesCommand { get; private set; }

        public ReactiveCommand<ChartSeries, System.Reactive.Unit> ShowChartCommand { get; private set; }

        public Interaction<ChartSeries, System.Reactive.Unit> ShowChartInteraction { get; }
            = new Interaction<ChartSeries, System.Reactive.Unit>(RxApp.MainThreadScheduler);

        public ReactiveCommand<System.Reactive.Unit, System.Reactive.Unit> ToggleExecutionCommand { get; private set; }

        public ReactiveCommand<OperationViewModel, Option<OperationViewModel>> AskDeleteOperationCommand { get; private set; }

        public Interaction<OperationViewModel, Option<OperationViewModel>> AskDeleteOperationInteraction { get; }
            = new Interaction<OperationViewModel, Option<OperationViewModel>>(RxApp.MainThreadScheduler);

        public ReactiveCommand<OperationViewModel, System.Reactive.Unit> DeleteOperationCommand { get; private set; }

        public ReactiveCommand<OperationViewModel, Option<OperationViewModel>> EditOperationCommand { get; private set; }

        public ReactiveCommand<System.Reactive.Unit, string> PickImportFileCommand { get; private set; }

        public Interaction<System.Reactive.Unit, string> PickImportFileInteraction { get; }
            = new Interaction<System.Reactive.Unit, string>(RxApp.MainThreadScheduler);

        public ReactiveCommand<string, IEnumerable<OperationViewModel>> ParseCsvCommand { get; private set; }

        public ReactiveCommand<IEnumerable<OperationViewModel>, IEnumerable<OperationViewModel>> ChooseOperationsCommand { get; private set; }

        public Interaction<OperationsImportViewModel, Option<OperationsImportViewModel>> ChooseOperationsInteraction { get; }
            = new Interaction<OperationsImportViewModel, Option<OperationsImportViewModel>>(
                RxApp.MainThreadScheduler);

        public AccountsViewModel(IngotsViewModel ingotsViewModel)
        {
            HostScreen = ingotsViewModel ?? Locator.Current.GetService<IScreen>();
            Activator = new ViewModelActivator();

            IngotsViewModel = ingotsViewModel;

            InitializeCommands(this);

            var filter = this.WhenAnyValue(x => x.SelectedAccount)
                             .Select(_ =>
                                 new Func<OperationViewModel, bool>(ovm =>
                                     SelectedAccount != null && ovm.AccountId == SelectedAccount.Id));

            var searchFilter = this.WhenAnyValue(x => x.SearchText)
                                   .Select(CreateSearchFilter);

            IngotsViewModel.AccountsCache
                           .Connect()
                           .Sort(SortExpressionComparer<AccountViewModel>.Ascending(a => a.Bank))
                           .Group(avm => avm.Bank)
                           .Transform(g => new BankViewModel(this, g.Cache) {Name = g.Key})
                           .ObserveOn(RxApp.MainThreadScheduler)
                           .Bind(out _banks)
                           .DisposeMany()
                           .SubscribeSafe();

            var operationsCache = IngotsViewModel.TransactionsCache.Connect()
                                                 .Transform(t => t as OperationViewModel)
                                                 .Or(IngotsViewModel.TransfersCache.Connect()
                                                                    .AutoRefresh(x => x.IsExecuted)
                                                                    .TransformMany(
                                                                        t => new[]
                                                                             {
                                                                                 t as OperationViewModel,
                                                                                 t.OppositeTransfer() as
                                                                                     OperationViewModel
                                                                             },
                                                                        t => (t.Id, t.IsDerived, typeof(TransferViewModel))))
                                                 .Filter(filter)
                                                 .AsObservableCache();

            operationsCache.Connect()
                           .Filter(searchFilter)
                           .Sort(SortExpressionComparer<OperationViewModel>.Descending(a => a.Date))
                           .ObserveOn(RxApp.MainThreadScheduler)
                           .Bind(out _operations)
                           .DisposeMany()
                           .SubscribeSafe();

            this.WhenActivated(disposables =>
            {
                this.WhenAnyValue(x => x.DrawnMonths)
                    .Select(_ => _chartSeries)
                    .WhereNotNull()
                    .InvokeCommand(ShowChartCommand)
                    .DisposeWith(disposables);

                operationsCache.Connect().AutoRefresh()
                               .CombineLatest(IngotsViewModel.AccountsCache.Connect().WhenValueChanged(x => x.StartValue),
                                   (_, __) => operationsCache.Items.ToArray())
                               .Throttle(TimeSpan.FromMilliseconds(100))
                               .InvokeCommand(BuildChartSeriesCommand)
                               .DisposeWith(disposables);
            });
        }

        private static void InitializeCommands(AccountsViewModel @this)
        {
            @this.SaveAccountCommand = ReactiveCommand.CreateFromObservable<AccountViewModel, AccountViewModel>(avm =>
                Observable.Start(() =>
                {
                    if (avm.Id == 0) // Insert
                    {
                        avm.Id = !@this.IngotsViewModel.AccountsCache.Items.Any()
                            ? 1
                            : @this.IngotsViewModel.AccountsCache.Items.Max(x => x.Id) + 1;
                        DataManager.Instance.InsertAccount(avm.ToData());
                    }
                    else // Update
                    {
                        DataManager.Instance.UpdateAccount(avm.ToData());
                    }

                    return avm;
                }));
            @this.SaveAccountCommand
                 .SubscribeSafe(avm => @this.IngotsViewModel.AccountsCache.AddOrUpdate(avm));
            @this.SaveAccountCommand
                 .ThrownExceptions
                 .InvokeCommand(@this.IngotsViewModel.LoggingViewModel, x => x.LogExceptionCommand);

            @this.AddAccountCommand =
                ReactiveCommand.CreateFromObservable(() => @this.EditAccountInteraction.Handle(new AccountViewModel()));
            @this.AddAccountCommand
                 .SubscribeSafe(option =>
                 {
                     option.Match(avm => Observable.Return(avm).InvokeCommand(@this, x => x.SaveAccountCommand)
                         , () => { });
                 });
            @this.AddAccountCommand
                 .ThrownExceptions
                 .InvokeCommand(@this.IngotsViewModel.LoggingViewModel, x => x.LogExceptionCommand);

            @this.EditAccountCommand = ReactiveCommand.CreateFromObservable<AccountViewModel, Option<AccountViewModel>>(
                avm
                    => @this.EditAccountInteraction.Handle((AccountViewModel) avm.Clone()),
                @this.WhenAnyValue(o => o.SelectedAccount).Select(o => o != null).ObserveOn(RxApp.MainThreadScheduler));
            @this.EditAccountCommand
                 .SubscribeSafe(option =>
                 {
                     option.Match(avm => Observable.Return(avm).InvokeCommand(@this, x => x.SaveAccountCommand)
                         , () => { });
                 });
            @this.EditAccountCommand
                 .ThrownExceptions
                 .InvokeCommand(@this.IngotsViewModel.LoggingViewModel, x => x.LogExceptionCommand);

            @this.SaveTransactionCommand =
                ReactiveCommand.CreateFromObservable<TransactionViewModel, TransactionViewModel>(tvm =>
                    Observable.Start(() =>
                    {
                        if (tvm.Id == 0) // Insert
                        {
                            tvm.Id = !@this.IngotsViewModel.TransactionsCache.Items.Any()
                                ? 1
                                : @this.IngotsViewModel.TransactionsCache.Items.Max(x => x.Id) + 1;
                            DataManager.Instance.InsertOperations(tvm.ToData());
                        }
                        else // Update
                        {
                            DataManager.Instance.UpdateOperation(tvm.ToData());
                        }

                        return tvm;
                    }));
            @this.SaveTransactionCommand
                 .SubscribeSafe(tvm => @this.IngotsViewModel.TransactionsCache.AddOrUpdate(tvm));
            @this.SaveTransactionCommand
                 .ThrownExceptions
                 .InvokeCommand(@this.IngotsViewModel.LoggingViewModel, x => x.LogExceptionCommand);

            @this.AddTransactionCommand = ReactiveCommand.CreateFromObservable(() =>
                {
                    var categories = @this.Operations
                                          .OfType<TransactionViewModel>()
                                          .Where(t => t.AccountId == @this.SelectedAccount.Id &&
                                                      !string.IsNullOrWhiteSpace(t.Category))
                                          .Select(t => (t.Category, t.SubCategory))
                                          .GroupBy(t => t.Category)
                                          .ToDictionary(g => g.Key, g => g.Select(t => t.SubCategory)
                                                                          .Where(s => !string.IsNullOrWhiteSpace(s))
                                                                          .Distinct()
                                                                          .ToArray());

                    var shops = @this.Operations
                                     .OfType<TransactionViewModel>()
                                     .Where(t => t.AccountId == @this.SelectedAccount.Id)
                                     .Select(t => t.Shop)
                                     .Where(s => !string.IsNullOrEmpty(s))
                                     .OrderBy(s => s)
                                     .Distinct()
                                     .ToArray();

                    var tvm = new TransactionViewModel
                              {AccountId = @this.SelectedAccount.Id, Categories = categories, Shops = shops};
                    return @this.EditTransactionInteraction.Handle(tvm);
                }
                , @this.WhenAnyValue(x => x.SelectedAccount).Select(a => a != null).ObserveOn(RxApp.MainThreadScheduler));
            @this.AddTransactionCommand
                 .SubscribeSafe(option =>
                 {
                     option.Match(tvm => Observable.Return(tvm).InvokeCommand(@this, x => x.SaveTransactionCommand)
                         , () => { });
                 });
            @this.AddTransactionCommand
                 .ThrownExceptions
                 .InvokeCommand(@this.IngotsViewModel.LoggingViewModel, x => x.LogExceptionCommand);

            @this.SaveTransferCommand = ReactiveCommand.CreateFromObservable<TransferViewModel, TransferViewModel>(
                tvm => Observable.Start(() =>
                {
                    if (tvm.Id == 0) // Insert
                    {
                        tvm.Id = !@this.IngotsViewModel.TransfersCache.Items.Any()
                            ? 1
                            : @this.IngotsViewModel.TransfersCache.Items.Max(x => x.Id) + 1;
                        DataManager.Instance.InsertOperations(tvm.ToData());
                    }
                    else // Update
                    {
                        DataManager.Instance.UpdateOperation(tvm.ToData());
                    }

                    return tvm;
                }));
            @this.SaveTransferCommand
                 .SubscribeSafe(tvm => { @this.IngotsViewModel.TransfersCache.AddOrUpdate(tvm); });
            @this.SaveTransferCommand
                 .ThrownExceptions
                 .InvokeCommand(@this.IngotsViewModel.LoggingViewModel, x => x.LogExceptionCommand);

            @this.AddTransferCommand = ReactiveCommand.CreateFromObservable(() =>
                {
                    var tvm = new TransferViewModel
                              {
                                  AccountId = @this.SelectedAccount.Id,
                                  AvailableTargets = @this.IngotsViewModel.AccountsCache.Items
                                                          .Where(a => a.Id != @this.SelectedAccount.Id).ToArray()
                              };
                    return @this.EditTransferInteraction.Handle(tvm);
                }
                , @this.WhenAnyValue(x => x.SelectedAccount).Select(a => a != null).ObserveOn(RxApp.MainThreadScheduler));
            @this.AddTransferCommand
                 .SubscribeSafe(option =>
                 {
                     option.Match(tvm => Observable.Return(tvm).InvokeCommand(@this, x => x.SaveTransferCommand)
                         , () => { });
                 });
            @this.AddTransferCommand
                 .ThrownExceptions
                 .InvokeCommand(@this.IngotsViewModel.LoggingViewModel, x => x.LogExceptionCommand);

            @this.ToggleExecutionCommand = ReactiveCommand.CreateFromObservable(() =>
                    Observable.Start(() =>
                    {
                        if (!@this.SelectedOperation.IsDerived)
                        {
                            @this.SelectedOperation.IsExecuted = !@this.SelectedOperation.IsExecuted;
                            DataManager.Instance.UpdateOperation(@this.SelectedOperation.ToData());
                        }
                        else
                        {
                            var operation = @this.IngotsViewModel.TransfersCache.Items.First(o =>
                                o.Id == @this.SelectedOperation.Id && !o.IsDerived);
                            operation.IsExecuted = !@this.SelectedOperation.IsExecuted;
                            DataManager.Instance.UpdateOperation(operation.ToData());
                        }
                    })
                , @this.WhenAnyValue(x => x.SelectedOperation).Select(a => a != null).ObserveOn(RxApp.MainThreadScheduler));
            @this.ToggleExecutionCommand
                 .ThrownExceptions
                 .InvokeCommand(@this.IngotsViewModel.LoggingViewModel, x => x.LogExceptionCommand);

            @this.ShowChartCommand = ReactiveCommand.CreateFromObservable<ChartSeries, System.Reactive.Unit>(input =>
            {
                var cs = (ChartSeries) input.Clone();

                if (cs.DataSource is Dictionary<DateTime, double> dct && dct.Count > 1)
                {
                    var data = dct.Where(x => x.Key >= DateTime.Today.AddMonths(-@this.DrawnMonths)).ToArray();
                    if (data.Length > 1)
                        cs.DataSource = data.ToDictionary(o => o.Key, o => o.Value);
                    else
                        cs.DataSource = dct.OrderByDescending(x => x.Key).Take(2).OrderBy(x => x.Key)
                                           .ToDictionary(o => o.Key, o => o.Value);
                }

                return @this.ShowChartInteraction.Handle(cs);
            });
            @this.ShowChartCommand
                 .ThrownExceptions
                 .InvokeCommand(@this.IngotsViewModel.LoggingViewModel, x => x.LogExceptionCommand);

            @this.BuildChartSeriesCommand = ReactiveCommand.CreateFromObservable<OperationViewModel[], ChartSeries>(input => Observable.Start(() =>
            {
                var data = new LinkedList<(DateTime date, double amount)>();

                input.Where(o => o.IsExecuted)
                     .OrderBy(o => o.Date)
                     .GroupBy(o => o.Date)
                     .Aggregate(data, (list, current) =>
                     {
                         var previous = list.Count == 0 ? (@this.SelectedAccount?.StartValue ?? 0d) : list.Last.Value.amount;
                         list.AddLast((current.Key, previous + current.Sum(o => o.Value)));
                         return list;
                     });

                if (data.Count > 0)
                {
                    var (date, amount) = data.Last.Value;
                    if (date < DateTime.Today)
                        data.AddLast((DateTime.Today, amount));
                }

                @this._chartSeries = new ChartSeries {Title = @this.SelectedAccount?.Description, DataSource = data.ToDictionary(o => o.date, o => o.amount)};
                return @this._chartSeries;
            }));

            @this.BuildChartSeriesCommand
                 .ThrownExceptions
                 .InvokeCommand(@this.IngotsViewModel.LoggingViewModel, x => x.LogExceptionCommand);
            @this.BuildChartSeriesCommand.InvokeCommand(@this, x => x.ShowChartCommand);

            @this.DeleteOperationCommand =
                ReactiveCommand.CreateFromObservable<OperationViewModel, System.Reactive.Unit>(ovm =>
                    Observable.Start(() =>
                    {
                        DataManager.Instance.DeleteOperation(ovm.ToData());

                        if (ovm is TransactionViewModel transaction)
                            @this.IngotsViewModel.TransactionsCache.Remove(transaction);
                        else if (ovm is TransferViewModel transfer)
                            @this.IngotsViewModel.TransfersCache.Remove(transfer);
                    }));
            @this.DeleteOperationCommand
                 .ThrownExceptions
                 .InvokeCommand(@this.IngotsViewModel.LoggingViewModel, x => x.LogExceptionCommand);

            @this.AskDeleteOperationCommand =
                ReactiveCommand.CreateFromObservable<OperationViewModel, Option<OperationViewModel>>(ovm
                        => @this.AskDeleteOperationInteraction.Handle(ovm),
                    @this.WhenAnyValue(o => o.SelectedOperation).Select(o => o != null && !o.IsDerived));
            @this.AskDeleteOperationCommand
                 .ThrownExceptions
                 .InvokeCommand(@this.IngotsViewModel.LoggingViewModel, x => x.LogExceptionCommand);
            @this.AskDeleteOperationCommand
                 .SubscribeSafe(option =>
                 {
                     option.Match(ovm => Observable.Return(ovm).InvokeCommand(@this, x => x.DeleteOperationCommand)
                         , () => { });
                 });

            @this.EditOperationCommand =
                ReactiveCommand.CreateFromObservable<OperationViewModel, Option<OperationViewModel>>(ovm =>
                {
                    if (ovm is TransactionViewModel transactionViewModel)
                    {
                        var categories = @this.Operations
                                              .OfType<TransactionViewModel>()
                                              .Where(t => t.AccountId == @this.SelectedAccount.Id &&
                                                          !string.IsNullOrWhiteSpace(t.Category))
                                              .Select(t => (t.Category, t.SubCategory))
                                              .GroupBy(t => t.Category)
                                              .ToDictionary(g => g.Key, g => g.Select(t => t.SubCategory)
                                                                              .Where(
                                                                                  s => !string.IsNullOrWhiteSpace(s))
                                                                              .Distinct()
                                                                              .ToArray());

                        var shops = @this.Operations
                                         .OfType<TransactionViewModel>()
                                         .Where(t => t.AccountId == @this.SelectedAccount.Id)
                                         .Select(t => t.Shop)
                                         .Where(s => !string.IsNullOrEmpty(s))
                                         .OrderBy(s => s)
                                         .Distinct()
                                         .ToArray();

                        var tvm = (TransactionViewModel) transactionViewModel.Clone();
                        tvm.Categories = categories;
                        tvm.Shops = shops;
                        return @this.EditTransactionInteraction.Handle(tvm)
                                    .Select(o => o.Match(x => Option<OperationViewModel>.Some(x),
                                        () => Option<OperationViewModel>.None));
                    }
                    else
                    {
                        var tvm = (TransferViewModel) ovm.Clone();
                        tvm.AvailableTargets = @this.IngotsViewModel.AccountsCache.Items
                                                    .Where(a => a.Id != @this.SelectedAccount.Id).ToArray();
                        return @this.EditTransferInteraction.Handle(tvm)
                                    .Select(o => o.Match(x => Option<OperationViewModel>.Some(x),
                                        () => Option<OperationViewModel>.None));
                    }
                }, @this.WhenAnyValue(o => o.SelectedOperation).Select(o => o != null && !o.IsDerived));
            @this.EditOperationCommand
                 .ThrownExceptions
                 .InvokeCommand(@this.IngotsViewModel.LoggingViewModel, x => x.LogExceptionCommand);

            @this.EditOperationCommand
                 .SubscribeSafe(o => o.Match(ovm =>
                     {
                         switch (ovm)
                         {
                             case TransactionViewModel transactionViewModel:
                                 Observable.Return(transactionViewModel)
                                           .InvokeCommand(@this, x => x.SaveTransactionCommand);
                                 break;

                             case TransferViewModel transferViewModel:
                                 Observable.Return(transferViewModel).InvokeCommand(@this, x => x.SaveTransferCommand);
                                 break;
                         }
                     },
                     () => { }));

            @this.SaveTransactionsCommand = ReactiveCommand.CreateFromObservable<IEnumerable<TransactionViewModel>, IEnumerable<TransactionViewModel>>(
                input => Observable.Start(() =>
                {
                    var id = !@this.IngotsViewModel.TransactionsCache.Items.Any()
                        ? 1
                        : @this.IngotsViewModel.TransactionsCache.Items.Max(x => x.Id);

                    var transactionViewModels = input as TransactionViewModel[] ?? input.ToArray();
                    foreach (var tvm in transactionViewModels)
                        tvm.Id = ++id;

                    DataManager.Instance.InsertOperations(transactionViewModels.Select(o => o.ToData()).ToArray());
                    return transactionViewModels;
                }));
            @this.SaveTransactionsCommand
                 .ThrownExceptions
                 .InvokeCommand(@this.IngotsViewModel.LoggingViewModel, x => x.LogExceptionCommand);
            @this.SaveTransactionsCommand
                 .SubscribeSafe(o => @this.IngotsViewModel.TransactionsCache.AddOrUpdate(o));

            @this.SaveTransfersCommand = ReactiveCommand.CreateFromObservable<IEnumerable<TransferViewModel>, IEnumerable<TransferViewModel>>(
                input => Observable.Start(() =>
                {
                    var id = !@this.IngotsViewModel.TransfersCache.Items.Any()
                        ? 1
                        : @this.IngotsViewModel.TransfersCache.Items.Max(x => x.Id);

                    var transferViewModels = input as TransferViewModel[] ?? input.ToArray();
                    foreach (var tvm in transferViewModels)
                        tvm.Id = ++id;

                    DataManager.Instance.InsertOperations(transferViewModels.Select(o => o.ToData()).ToArray());
                    return transferViewModels;
                }));
            @this.SaveTransfersCommand
                 .ThrownExceptions
                 .InvokeCommand(@this.IngotsViewModel.LoggingViewModel, x => x.LogExceptionCommand);
            @this.SaveTransfersCommand
                 .SubscribeSafe(o => @this.IngotsViewModel.TransfersCache.AddOrUpdate(o));

            @this.ChooseOperationsCommand =
                ReactiveCommand.CreateFromTask<IEnumerable<OperationViewModel>, IEnumerable<OperationViewModel>>(
                    input => Task.Run<IEnumerable<OperationViewModel>>(async () =>
                    {
                        var operationsImportViewModel = new OperationsImportViewModel();
                        operationsImportViewModel.OperationsCache.AddOrUpdate(input);
                        var res = await @this.ChooseOperationsInteraction.Handle(operationsImportViewModel);
                        return res.Match(x => x.SelectedOperations.ToArray(), () => new OperationViewModel[0]);
                    }));

            @this.ChooseOperationsCommand.SubscribeSafe(operations =>
            {
                var operationViewModels = operations as OperationViewModel[] ?? operations.ToArray();

                Observable.Return(operationViewModels.OfType<TransactionViewModel>())
                          .InvokeCommand(@this, x => x.SaveTransactionsCommand);

                Observable.Return(operationViewModels.OfType<TransferViewModel>())
                          .InvokeCommand(@this, x => x.SaveTransfersCommand);
            });

            @this.ParseCsvCommand = ReactiveCommand.CreateFromObservable<string, IEnumerable<OperationViewModel>>(
                path => Observable.Start(() =>
                {
                    var accounts = @this.IngotsViewModel.AccountsCache.Items
                                        //.Where(o => o.Id != @this.SelectedAccount.Id)
                                        .ToDictionary(o => o.Iban);
                    // Read config
                    var jsonFile = Path.Combine(Path.GetDirectoryName(Assembly.GetEntryAssembly().Location), "parsers.json");
                    var parser = JsonConvert.DeserializeObject<BankParser[]>(File.ReadAllText(jsonFile))
                                            .FirstOrDefault(x => x.Bank.Equals(@this.SelectedAccount.Bank));

                    if (parser == null)
                        return new OperationViewModel[0];

                    return File.ReadAllLines(path).Skip(1).Select<string, OperationViewModel>(l =>
                    {
                        var parts = l.Split(parser.Separator)
                                     .Select(o => parser.Quotes ? o.Replace('"', ' ').Trim() : o)
                                     .ToArray();
                        var reference = parts[parser.Reference];
                        var date = DateTime.Parse(parts[parser.Date]);
                        var value = double.Parse(parts[parser.Value]);
                        var counterPart = IbanUtils.UnFormat(parts[parser.CounterPart]);
                        var description = parts[parser.Description];
                        var iban = parser.Iban != -1 ? parts[parser.Iban] : @this.SelectedAccount.Iban;

                        if (accounts.TryGetValue(iban, out var account))
                        {
                            if (IbanUtils.IsValid(counterPart)
                                && accounts.TryGetValue(counterPart, out var target)
                                && !target.Equals(@this.SelectedAccount))
                            {
                                return new TransferViewModel
                                       {
                                           ImportReference = reference,
                                           AccountId = account.Id,
                                           TargetId = target.Id,
                                           Date = date,
                                           Value = value,
                                           Description = !string.IsNullOrWhiteSpace(description)
                                               ? description
                                               : counterPart,
                                           IsExecuted = true
                                       };
                            }
                            else
                            {
                                return new TransactionViewModel
                                       {
                                           ImportReference = reference,
                                           AccountId = account.Id,
                                           Date = date,
                                           Value = value,
                                           Description = !string.IsNullOrWhiteSpace(description)
                                               ? description
                                               : counterPart,
                                           IsExecuted = true
                                       };
                            }
                        }

                        return null;
                    }).Where(x => x != null).ToArray();
                }));
            @this.ParseCsvCommand
                 .InvokeCommand(@this, x => x.ChooseOperationsCommand);
            @this.ParseCsvCommand
                 .ThrownExceptions
                 .InvokeCommand(@this.IngotsViewModel.LoggingViewModel, x => x.LogExceptionCommand);

            @this.PickImportFileCommand = ReactiveCommand.CreateFromObservable(() =>
                    @this.PickImportFileInteraction.Handle(System.Reactive.Unit.Default),
                @this.WhenAnyValue(o => o.SelectedAccount).Select(a => a != null).ObserveOn(RxApp.MainThreadScheduler));
            @this.PickImportFileCommand.InvokeCommand(@this, x => x.ParseCsvCommand);
            @this.PickImportFileCommand
                 .ThrownExceptions
                 .InvokeCommand(@this.IngotsViewModel.LoggingViewModel, x => x.LogExceptionCommand);
        }

        public Func<OperationViewModel, bool> CreateSearchFilter(string searchText)
            => ovm =>
            {
                if (string.IsNullOrEmpty(searchText))
                    return true;

                var searched = searchText.Trim();
                var filters = new List<Predicate<OperationViewModel>>();

                if (searched.StartsWith("<") && double.TryParse(searched.Substring(1), out double val))
                    filters.Add(x => x.Value < val);
                else if (searched.StartsWith(">") && double.TryParse(searched.Substring(1), out val))
                    filters.Add(x => x.Value > val);
                else if (int.TryParse(searched, out int year))
                    filters.Add(x => x.Date.Year == year);
                else if (searched.Split('-').Length == 2)
                {
                    var split = searched.Split('-');
                    if (int.TryParse(split[0], out year) && int.TryParse(split[1], out int month))
                        filters.Add(x => x.Date.Year == year && x.Date.Month == month);
                }
                else if (DateTime.TryParse(searched, out var dt))
                    filters.Add(x => x.Date.Equals(dt));
                else
                    filters.Add(x => x.Description.StartsWith(searched, StringComparison.CurrentCultureIgnoreCase)
                                     || (x as TransactionViewModel)?.Category.StartsWith(searched,
                                         StringComparison.CurrentCultureIgnoreCase) ==
                                     true
                                     || (x as TransactionViewModel)?.SubCategory.StartsWith(searched,
                                         StringComparison.CurrentCultureIgnoreCase) ==
                                     true
                                     || (x as TransactionViewModel)?.Shop.StartsWith(searched,
                                         StringComparison.CurrentCultureIgnoreCase) ==
                                     true);

                return filters.All(x => x(ovm));
            };

        public ViewModelActivator Activator { get; }
        public string UrlPathSegment => "Accounts";
        public IScreen HostScreen { get; }
    }
}