﻿using DynamicData;
using DynamicData.Binding;
using ReactiveUI;
using System;
using System.Collections.ObjectModel;
using System.Reactive;
using System.Reactive.Disposables;
using System.Reactive.Linq;

namespace IngotsRx.ViewModels
{
    public class LoggingViewModel : ReactiveObject, IActivatableViewModel
    {
        public IngotsViewModel IngotsViewModel { get; }

        private SourceCache<EventLogViewModel, DateTime> LogsCache { get; } = new SourceCache<EventLogViewModel, DateTime>(x => x.TimeStamp);
        private ReadOnlyObservableCollection<EventLogViewModel> _logs;
        public ReadOnlyObservableCollection<EventLogViewModel> Logs => _logs;

        public ReactiveCommand<(EventKind, string), EventLogViewModel> LogMessageCommand { get; private set; }

        public ReactiveCommand<Exception, (EventKind, string)> LogExceptionCommand { get; private set; }

        public ReactiveCommand<EventLogViewModel, Unit> ShowMessageCommand { get; private set; }

        public Interaction<EventLogViewModel, Unit> ShowMessageInteraction { get; }
            = new Interaction<EventLogViewModel, Unit>(RxApp.MainThreadScheduler);

        public ViewModelActivator Activator { get; }

        public LoggingViewModel(IngotsViewModel ingotsViewModel)
        {
            Activator = new ViewModelActivator();
            IngotsViewModel = ingotsViewModel;

            InitializeCommands(this);

            this.WhenActivated(disposables =>
            {
                LogsCache.Connect()
                    .Sort(SortExpressionComparer<EventLogViewModel>.Descending(l => l.TimeStamp))
                    .ObserveOn(RxApp.MainThreadScheduler)
                    .Bind(out _logs)
                    .DisposeMany()
                    .Subscribe()
                    .DisposeWith(disposables);
            });
        }

        private static void InitializeCommands(LoggingViewModel @this)
        {
            @this.ShowMessageCommand = ReactiveCommand.CreateFromObservable<EventLogViewModel, Unit>(elvm => @this.ShowMessageInteraction.Handle(elvm));

            @this.LogMessageCommand = ReactiveCommand.CreateFromObservable<(EventKind kind, string message), EventLogViewModel>(input
                 => Observable.Start(() =>
                 {
                     var (kind, message) = input;
                     var elvm = new EventLogViewModel
                     {
                         Kind = kind,
                         Message = message,
                         TimeStamp = DateTime.Now
                     };
                     @this.LogsCache.AddOrUpdate(elvm);
                     return elvm;
                 }));
            @this.LogMessageCommand.InvokeCommand(@this, x => x.ShowMessageCommand);

            @this.LogExceptionCommand = ReactiveCommand.CreateFromObservable<Exception, (EventKind, string)>(exc =>
               Observable.Start(() => (EventKind.Error, exc.Message)));
            @this.LogExceptionCommand.InvokeCommand(@this, x => x.LogMessageCommand);
        }
    }
}