﻿using ReactiveUI;
using ReactiveUI.Fody.Helpers;
using System;

namespace IngotsRx.ViewModels
{
    public enum EventKind { Info, Warn, Error }

    public class EventLogViewModel : ReactiveObject
    {
        [Reactive] public EventKind Kind { get; set; }
        [Reactive] public string Message { get; set; }
        [Reactive] public DateTime TimeStamp { get; set; }
    }
}