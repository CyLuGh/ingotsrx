﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Reactive.Disposables;
using System.Reactive.Linq;
using DynamicData;
using DynamicData.Alias;
using ReactiveUI;
using ReactiveUI.Fody.Helpers;

namespace IngotsRx.ViewModels
{
    public class StashViewModel : ReactiveObject, IActivatableViewModel
    {
        [Reactive] public string Name { get; set; }
        public double Value { [ObservableAsProperty] get; }
        [Reactive] public IObservableCache<AccountViewModel, long> ComponentsCache { get; set; }

        private ReadOnlyObservableCollection<AccountViewModel> _components;
        public ReadOnlyObservableCollection<AccountViewModel> Components => _components;

        public StashViewModel()
        {
            Activator = new ViewModelActivator();

            this.WhenActivated(disposables =>
            {
                ComponentsCache.Connect()
                               .ObserveOn(RxApp.MainThreadScheduler)
                               .Bind(out _components)
                               .DisposeMany()
                               .Subscribe()
                               .DisposeWith(disposables);

                ComponentsCache.Connect()
                          .AutoRefresh(x => x.StartValue)
                          .AutoRefresh(x => x.OperationsValue)
                          .Select(_ => ComponentsCache.Items.Sum(o => o.StartValue + o.OperationsValue))
                          .ObserveOn(RxApp.MainThreadScheduler)
                          .ToPropertyEx(this, x => x.Value)
                          .DisposeWith(disposables);
            });
        }

        public ViewModelActivator Activator { get; }
    }
}