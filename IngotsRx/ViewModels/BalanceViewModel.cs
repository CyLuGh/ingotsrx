﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Reactive.Disposables;
using System.Reactive.Linq;
using DynamicData;
using DynamicData.Binding;
using ReactiveUI;
using ReactiveUI.Fody.Helpers;

namespace IngotsRx.ViewModels
{
    public class BalanceViewModel : ReactiveObject, IActivatableViewModel
    {
        public ViewModelActivator Activator { get; }

        [Reactive] public double Income { get; set; }
        [Reactive] public double Expense { get; set; }
        [Reactive] public DateTime Period { get; set; }

        public double Balance { [ObservableAsProperty] get; }
        public IObservableCache<BalanceViewModel, DateTime> ComponentsCache { get; set; }
        
        [Reactive] public BalanceViewModel[] Balances { get; set; }
    
        public BalanceViewModel()
        {
            Activator = new ViewModelActivator();

            this.WhenActivated(disposables =>
            {
                this.WhenAnyValue(x => x.Income)
                    .CombineLatest(this.WhenAnyValue(x => x.Expense),
                        (i, e) => i - e)
                    .Throttle(TimeSpan.FromMilliseconds(100))
                    .ObserveOn(RxApp.MainThreadScheduler)
                    .ToPropertyEx(this, x => x.Balance)
                    .DisposeWith(disposables);

                // I'll never know why the Bind wouldn't work... so let's trick it
                ComponentsCache?.Connect()
                               .Subscribe(_ =>
                               {
                                   Income = ComponentsCache.Items.Sum(o => o.Income);
                                   Expense = ComponentsCache.Items.Sum(o => o.Expense);
                                   Balances = ComponentsCache.Items.OrderByDescending(x=>x.Period).ToArray();
                               })
                               .DisposeWith(disposables);
                
            });
        }
    }
}