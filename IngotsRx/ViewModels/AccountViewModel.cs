﻿using IngotsRx.Data;
using ReactiveUI;
using ReactiveUI.Fody.Helpers;
using ReactiveUI.Validation.Extensions;
using ReactiveUI.Validation.Helpers;
using System;
using System.Diagnostics.CodeAnalysis;
using System.Reactive.Disposables;
using System.Reactive.Linq;

namespace IngotsRx.ViewModels
{
    public class AccountViewModel : ReactiveValidationObject<AccountsViewModel>, IActivatableViewModel, ICloneable, IEquatable<AccountViewModel>
    {
        [Reactive]
        public long Id { get; set; }

        [Reactive]
        public string Bank { get; set; }

        [Reactive]
        public string Bic { get; set; }

        [Reactive]
        public string Description { get; set; }

        [Reactive]
        public string Iban { get; set; }

        [Reactive]
        public AccountKind Kind { get; set; } = AccountKind.Checking;

        [Reactive]
        public string Stash { get; set; }

        [Reactive]
        public double StartValue { get; set; }

        [Reactive]
        public string StartValueInput { get; set; }

        [Reactive]
        public double OperationsValue { get; set; }

        public double CurrentValue { [ObservableAsProperty]get; }

        public ValidationHelper IbanRule { get; }
        public ValidationHelper StartValueRule { get; }

        public AccountViewModel()
        {
            Activator = new ViewModelActivator();

            IbanRule = this.ValidationRule(x => x.Iban,
                iban => string.IsNullOrEmpty(iban) || IbanUtils.IsValid(iban),
                "Invalid IBAN!");

            StartValueRule = this.ValidationRule(x => x.StartValueInput,
                input => double.TryParse(input, out var _),
                "Input should be a valid numeric value!");

            this.WhenActivated(disposables =>
            {
                StartValueInput = StartValue.ToString("N2");

                this.WhenAnyValue(o => o.StartValue)
                    .CombineLatest(this.WhenAnyValue(o => o.OperationsValue),
                    (sv, ops) => sv + ops)
                    .Throttle(TimeSpan.FromMilliseconds(100))
                    .ObserveOn(RxApp.MainThreadScheduler)
                    .ToPropertyEx(this, x => x.CurrentValue)
                    .DisposeWith(disposables);

                this.WhenAnyValue(o => o.StartValueInput)
                    .SubscribeSafe(input =>
                    {
                        if (double.TryParse(input, out var value))
                            this.StartValue = value;
                        else this.StartValue = 0;
                    })
                    .DisposeWith(disposables);
            });
        }

        public AccountViewModel(Account account) : this()
        {
            Id = account.Id;
            Bank = account.Bank;
            Bic = account.Bic;
            Description = account.Description;
            Iban = account.Iban;
            Kind = account.Kind;
            StartValue = account.StartValue;
            Stash = account.Stash;
        }

        internal Account ToData()
           => new Account
           {
               Id = Id,
               Bank = Bank,
               Bic = Bic,
               Description = Description,
               Iban = Iban,
               Kind = Kind,
               StartValue = StartValue,
               Stash = Stash
           };

        public ViewModelActivator Activator { get; }

        public object Clone()
            => new AccountViewModel
            {
                Id = Id,
                Bank = Bank,
                Bic = Bic,
                Description = Description,
                Iban = Iban,
                Kind = Kind,
                StartValue = StartValue,
                OperationsValue = OperationsValue,
                Stash = Stash
            };

        public bool Equals([AllowNull] AccountViewModel other)
        {
            if (ReferenceEquals(null, other))
                return false;
            if (ReferenceEquals(this, other))
                return true;

            return Id.Equals(other.Id);
        }

        public override bool Equals(object obj)
            => Equals(obj as AccountViewModel);

        public override int GetHashCode()
            => Id.GetHashCode();

        public override string ToString()
            => $"Id: {Id}";
    }
}