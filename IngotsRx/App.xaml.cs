﻿using IngotsRx.ViewModels;
using IngotsRx.Views;
using ReactiveUI;
using Splat;
using System.Globalization;
using System.Windows;
using System.Windows.Markup;

namespace IngotsRx
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        public App()
        {
            FrameworkElement.LanguageProperty.OverrideMetadata(typeof(FrameworkElement), new FrameworkPropertyMetadata(XmlLanguage.GetLanguage(CultureInfo.CurrentCulture.IetfLanguageTag)));
            FrameworkElement.LanguageProperty.OverrideMetadata(typeof(System.Windows.Documents.TextElement), new FrameworkPropertyMetadata(XmlLanguage.GetLanguage(CultureInfo.CurrentCulture.IetfLanguageTag)));

            ConfigureLogs();

            //Locator.CurrentMutable.RegisterViewsForViewModels(Assembly.GetCallingAssembly());

            Locator.CurrentMutable.Register(() => new AccountsView(), typeof(IViewFor<AccountsViewModel>));
            Locator.CurrentMutable.Register(() => new ChartsView(), typeof(IViewFor<ChartsViewModel>));
            Locator.CurrentMutable.Register(() => new BalancesView(), typeof(IViewFor<BalancesViewModel>));
            Locator.CurrentMutable.Register(() => new LoggingView(), typeof(IViewFor<LoggingViewModel>));
            Locator.CurrentMutable.Register(() => new BankView(), typeof(IViewFor<BankViewModel>));
            Locator.CurrentMutable.Register(() => new AccountView(), typeof(IViewFor<AccountViewModel>));

            Locator.CurrentMutable.Register(() => new TransactionEditorPartView(), typeof(IViewFor<TransactionViewModel>), "Editor");
            Locator.CurrentMutable.Register(() => new TransferEditorPartView(), typeof(IViewFor<TransferViewModel>), "Editor");
            
            Locator.CurrentMutable.Register(() => new TransactionImportView(), typeof(IViewFor<TransactionViewModel>), "Import");
            Locator.CurrentMutable.Register(() => new TransferImportView(), typeof(IViewFor<TransferViewModel>), "Import");

            Locator.CurrentMutable.Register(() => new AccountInlineView(), typeof(IViewFor<AccountViewModel>), "Inline");
            Locator.CurrentMutable.Register(() => new AccountStashView(), typeof(IViewFor<AccountViewModel>), "Stash");

            Locator.CurrentMutable.Register(() => new BalanceTreeView(), typeof(IViewFor<BalanceViewModel>), "Monthly");
            Locator.CurrentMutable.Register(() => new YearlyBalanceTreeView(), typeof(IViewFor<BalanceViewModel>),"Yearly");

            Locator.CurrentMutable.Register(() => new EventLogView(), typeof(IViewFor<EventLogViewModel>));

            //Locator.CurrentMutable.RegisterConstant(new IdAccountTypeConverter(), typeof(IBindingTypeConverter));
        }

        private static void ConfigureLogs()
        {
            var config = new NLog.Config.LoggingConfiguration();

            // Targets where to log to: File and Console
            var logfile = new NLog.Targets.FileTarget("logfile")
            {
                FileName = "ingotsRx.log",
                DeleteOldFileOnStartup = true
            };
            var logconsole = new NLog.Targets.ConsoleTarget("logconsole");

            // Rules for mapping loggers to targets
            config.AddRule(NLog.LogLevel.Info, NLog.LogLevel.Fatal, logconsole);
            config.AddRule(NLog.LogLevel.Debug, NLog.LogLevel.Fatal, logfile);

            // Apply config
            NLog.LogManager.Configuration = config;
        }
    }
}