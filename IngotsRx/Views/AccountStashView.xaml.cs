﻿using System;
using System.Reactive.Disposables;
using System.Reactive.Linq;
using IngotsRx.Data;
using IngotsRx.ViewModels;
using MaterialDesignThemes.Wpf;
using ReactiveUI;

namespace IngotsRx.Views
{
    public partial class AccountStashView 
    {
        public AccountStashView()
        {
            InitializeComponent();
            
            this.WhenActivated(disposables =>
            {
                this.WhenAnyValue(v => v.ViewModel)
                    .Where(vm => vm != null)
                    .Do(x => PopulateFromViewModel(x, disposables))
                    .Subscribe()
                    .DisposeWith(disposables);
            });
        }

        private void PopulateFromViewModel(AccountViewModel accountViewModel, CompositeDisposable disposables)
        {
            this.OneWayBind(accountViewModel,
                    v => v.Kind,
                    vm => vm.KindIcon.Kind,
                    ConvertAccountKindToPackIconKind)
                .DisposeWith(disposables);

            this.OneWayBind(accountViewModel,
                    v => v.CurrentValue,
                    vm => vm.ValueTextBlock.Text,
                    d => d.ToString("C2"))
                .DisposeWith(disposables);

            this.OneWayBind(accountViewModel,
                    v => v.Description,
                    vm => vm.DescriptionTextBlock.Text)
                .DisposeWith(disposables);
        }
        
        private PackIconKind ConvertAccountKindToPackIconKind(AccountKind kind)
        {
            return kind switch
            {
                AccountKind.Checking => PackIconKind.Money,
                AccountKind.Saving => PackIconKind.Bank,
                AccountKind.Locked => PackIconKind.Lock,
                _ => PackIconKind.QuestionMark
            };
        }
    }
}