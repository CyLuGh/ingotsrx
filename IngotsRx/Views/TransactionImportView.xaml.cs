﻿using System.Reactive.Disposables;
using System.Reactive.Linq;
using System.Windows.Controls;
using IngotsRx.ViewModels;
using ReactiveUI;

namespace IngotsRx.Views
{
    public partial class TransactionImportView 
    {
        public TransactionImportView()
        {
            InitializeComponent();

            this.WhenActivated(disposables =>
            {
                this.WhenAnyValue(o => o.ViewModel)
                    .WhereNotNull()
                    .Do( PopulateFromViewModel)
                    .SubscribeSafe()
                    .DisposeWith(disposables);
            });
        }

        private void PopulateFromViewModel(TransactionViewModel transactionViewModel)
        {
            DateTextBlock.Text = transactionViewModel.Date.ToString("yyyy-MM-dd");
            DescriptionTextBlock.Text = transactionViewModel.Description;
            ValueTextBlock.Text = transactionViewModel.Value.ToString("C2");
        }
    }
}