﻿using IngotsRx.ViewModels;
using IngotsRx.Views.Converters;
using MaterialDesignThemes.Wpf;
using ReactiveUI;
using Splat;
using System;
using System.Linq;
using System.Reactive.Disposables;
using System.Reactive.Linq;

namespace IngotsRx.Views
{
    /// <summary>
    /// Interaction logic for TransferEditorPartView.xaml
    /// </summary>
    public partial class TransferEditorPartView
    {
        public TransferEditorPartView()
        {
            InitializeComponent();

            this.WhenActivated(disposables =>
            {
                this.WhenAnyValue(v => v.ViewModel)
                  .WhereNotNull()
                  .Do(x => PopulateFromViewModel(x, disposables))
                  .Subscribe()
                  .DisposeWith(disposables);
            });
        }

        private void PopulateFromViewModel(TransferViewModel tvm, CompositeDisposable disposables)
        {
            this.OneWayBind(tvm,
                vm => vm.AccountId,
                v => v._hostCurrentAccount.Content,
                id =>
                {
                    var viewLocator = Locator.Current.GetService<IViewLocator>();
                    var accountViewModel = (AccountViewModel)IdAccountConverter.Instance.Convert(id, typeof(AccountViewModel), null, null);
                    var view = viewLocator.ResolveView(accountViewModel, "Inline");
                    view.ViewModel = accountViewModel;
                    return view;
                })
                .DisposeWith(disposables);

            this.OneWayBind(tvm,
                vm => vm.Value,
                v => v._iconFlow.Kind,
                val => val <= 0 ? PackIconKind.ArrowDownBoldBox : PackIconKind.ArrowUpBoldBox)
                .DisposeWith(disposables);

            this.OneWayBind(tvm,
                vm => vm.AvailableTargets,
                v => v._comboTargets.ItemsSource)
                .DisposeWith(disposables);

            this.Bind(tvm,
                vm => vm.TargetId,
                v => v._comboTargets.SelectedItem,
                viewModelProperty => tvm.AvailableTargets.FirstOrDefault(x => x.Id == viewModelProperty),
                viewProperty => viewProperty is AccountViewModel avm ? avm.Id : 0)
                .DisposeWith(disposables);
        }
    }
}