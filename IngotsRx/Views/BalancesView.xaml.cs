﻿using System;
using System.Reactive.Disposables;
using System.Reactive.Linq;
using System.Windows.Controls;
using IngotsRx.ViewModels;
using ReactiveUI;

namespace IngotsRx.Views
{
    public partial class BalancesView 
    {
        public BalancesView()
        {
            InitializeComponent();

            this.WhenActivated(disposables =>
            {
                this.WhenAnyValue(x => x.ViewModel)
                    .WhereNotNull()
                    .Do(vm => PopulateFromViewModel(vm, disposables))
                    .SubscribeSafe()
                    .DisposeWith(disposables);
            });
        }

        private void PopulateFromViewModel(BalancesViewModel bvm, CompositeDisposable disposables)
        {
            this.OneWayBind(bvm,
                    vm => vm.TotalAvailable,
                    v => v.TotalAvailableRun.Text,
                    v=>v.ToString("C2"))
                .DisposeWith(disposables);
            
            this.OneWayBind(bvm,
                    vm => vm.AccountsCount,
                    v => v.AccountsCountRun.Text)
                .DisposeWith(disposables);

            this.OneWayBind(bvm,
                    vm => vm.Stashes,
                    v => v.StashesTreeView.ItemsSource)
                .DisposeWith(disposables);
            
            this.OneWayBind(bvm,
                    vm => vm.Balances,
                    v => v.BalancesTreeView.ItemsSource)
                .DisposeWith(disposables);
        }
    }
}