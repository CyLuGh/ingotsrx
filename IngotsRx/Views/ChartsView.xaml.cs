﻿using IngotsRx.ViewModels;
using IngotsRx.Views.Infrastructure;
using OxyPlot.Axes;
using ReactiveUI;
using System;
using System.Linq;
using System.Reactive;
using System.Reactive.Disposables;
using System.Reactive.Linq;
using System.Windows.Media;

namespace IngotsRx.Views
{
    public partial class ChartsView
    {
        public ChartsView()
        {
            InitializeComponent();

            _monthlyPlotView.Configure();
            (_monthlyPlotView.Model.Axes[0] as DateTimeAxis).IntervalType = DateTimeIntervalType.Months;
            _yearlyPlotView.Configure();
            (_yearlyPlotView.Model.Axes[0] as DateTimeAxis).IntervalType = DateTimeIntervalType.Years;

            this.WhenActivated(disposables =>
            {
                this.WhenAnyValue(v => v.ViewModel)
                   .Where(vm => vm != null)
                   .Do(x => PopulateFromViewModel(x, disposables))
                   .Subscribe()
                   .DisposeWith(disposables);
            });
        }

        private void PopulateFromViewModel(ChartsViewModel viewModel, CompositeDisposable disposables)
        {
            viewModel.AddMonthlyIncomeSeriesInteraction.RegisterHandler(context =>
            {
                var series = _monthlyPlotView.Model.Series.FirstOrDefault(s => s.Title.Equals(context.Input.Title));
                if (series != null)
                    _monthlyPlotView.Model.Series.Remove(series);

                _monthlyPlotView.Model.AddColumnSeries(context.Input, Brushes.ForestGreen, 0, 2);
                _monthlyPlotView.Model.InvalidatePlot(true);
                context.SetOutput(Unit.Default);
            })
                .DisposeWith(disposables);

            viewModel.AddMonthlyExpenseSeriesInteraction.RegisterHandler(context =>
            {
                var series = _monthlyPlotView.Model.Series.FirstOrDefault(s => s.Title.Equals(context.Input.Title));
                if (series != null)
                    _monthlyPlotView.Model.Series.Remove(series);

                _monthlyPlotView.Model.AddColumnSeries(context.Input, Brushes.IndianRed, 1, 2);
                _monthlyPlotView.Model.InvalidatePlot(true);
                context.SetOutput(Unit.Default);
            })
                .DisposeWith(disposables);

            viewModel.AddYearlyIncomeSeriesInteraction.RegisterHandler(context =>
            {
                var series = _yearlyPlotView.Model.Series.FirstOrDefault(s => s.Title.Equals(context.Input.Title));
                if (series != null)
                    _yearlyPlotView.Model.Series.Remove(series);

                _yearlyPlotView.Model.AddColumnSeries(context.Input, Brushes.ForestGreen, 0, 2, 150);
                _yearlyPlotView.Model.InvalidatePlot(true);
                context.SetOutput(Unit.Default);
            })
               .DisposeWith(disposables);

            viewModel.AddYearlyExpenseSeriesInteraction.RegisterHandler(context =>
            {
                var series = _yearlyPlotView.Model.Series.FirstOrDefault(s => s.Title.Equals(context.Input.Title));
                if (series != null)
                    _yearlyPlotView.Model.Series.Remove(series);

                _yearlyPlotView.Model.AddColumnSeries(context.Input, Brushes.IndianRed, 1, 2, 150);
                _yearlyPlotView.Model.InvalidatePlot(true);
                context.SetOutput(Unit.Default);
            })
                .DisposeWith(disposables);
        }
    }
}