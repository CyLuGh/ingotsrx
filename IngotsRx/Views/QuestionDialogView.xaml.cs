﻿using IngotsRx.ViewModels;
using ReactiveUI;
using System;
using System.Reactive.Disposables;
using System.Reactive.Linq;

namespace IngotsRx.Views
{
    public partial class QuestionDialogView
    {
        public QuestionDialogView()
        {
            InitializeComponent();

            this.WhenActivated(disposables =>
            {
                this.WhenAnyValue(v => v.ViewModel)
                  .WhereNotNull()
                  .Do(x => PopulateFromViewModel(x, disposables))
                  .Subscribe()
                  .DisposeWith(disposables);
            });
        }

        private void PopulateFromViewModel(QuestionDialogViewModel viewModel, CompositeDisposable disposables)
        {
            this.OneWayBind(viewModel,
                vm => vm.Title,
                v => v._txtTitle.Text)
                .DisposeWith(disposables);

            this.OneWayBind(viewModel,
                vm => vm.Message,
                v => v._txtMessage.Text)
                .DisposeWith(disposables);

            this.OneWayBind(viewModel,
               vm => vm.AcceptText,
               v => v._bAccept.Content)
               .DisposeWith(disposables);

            this.OneWayBind(viewModel,
               vm => vm.CancelText,
               v => v._bCancel.Content)
               .DisposeWith(disposables);

            this.BindCommand(viewModel,
                vm => vm.AcceptCommand,
                v => v._bAccept)
                .DisposeWith(disposables);

            this.BindCommand(viewModel,
                vm => vm.CancelCommand,
                v => v._bCancel)
                .DisposeWith(disposables);
        }
    }
}