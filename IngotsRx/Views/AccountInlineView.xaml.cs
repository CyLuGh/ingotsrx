﻿using IngotsRx.Data;
using IngotsRx.ViewModels;
using MaterialDesignThemes.Wpf;
using ReactiveUI;
using System;
using System.Reactive.Disposables;
using System.Reactive.Linq;

namespace IngotsRx.Views
{
    /// <summary>
    /// Interaction logic for AccountInlineView.xaml
    /// </summary>
    public partial class AccountInlineView
    {
        public AccountInlineView()
        {
            InitializeComponent();

            this.WhenActivated(disposables =>
            {
                this.WhenAnyValue(v => v.ViewModel)
                  .Where(vm => vm != null)
                  .Do(x => PopulateFromViewModel(x, disposables))
                  .Subscribe()
                  .DisposeWith(disposables);
            });
        }

        private void PopulateFromViewModel(AccountViewModel accountViewModel, CompositeDisposable disposables)
        {
            this.OneWayBind(accountViewModel,
                v => v.Kind,
                vm => vm.KindIcon.Kind,
                ConvertAccountKindToPackIconKind)
                .DisposeWith(disposables);

            this.OneWayBind(accountViewModel,
              v => v.Bank,
              vm => vm.BankTextBlock.Text,
              IbanUtils.Format)
              .DisposeWith(disposables);

            this.OneWayBind(accountViewModel,
                v => v.Iban,
                vm => vm.IbanTextBlock.Text,
                IbanUtils.Format)
                .DisposeWith(disposables);

            this.OneWayBind(accountViewModel,
                v => v.Description,
                vm => vm.DescriptionTextBlock.Text)
                .DisposeWith(disposables);
        }

        private PackIconKind ConvertAccountKindToPackIconKind(AccountKind kind)
        {
            return kind switch
            {
                AccountKind.Checking => PackIconKind.Money,
                AccountKind.Saving => PackIconKind.Bank,
                AccountKind.Locked => PackIconKind.Lock,
                _ => PackIconKind.QuestionMark
            };
        }
    }
}