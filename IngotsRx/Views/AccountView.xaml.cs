﻿using IngotsRx.Data;
using IngotsRx.ViewModels;
using MaterialDesignThemes.Wpf;
using ReactiveUI;
using System;
using System.Collections.Generic;
using System.Reactive.Disposables;
using System.Reactive.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace IngotsRx.Views
{
    /// <summary>
    /// Interaction logic for AccountView.xaml
    /// </summary>
    public partial class AccountView
    {
        public AccountView()
        {
            InitializeComponent();

            this.WhenActivated(disposables =>
            {
                this.WhenAnyValue(v => v.ViewModel)
                  .Where(vm => vm != null)
                  .Do(x => PopulateFromViewModel(x, disposables))
                  .Subscribe()
                  .DisposeWith(disposables);
            });
        }

        private void PopulateFromViewModel(AccountViewModel accountViewModel, CompositeDisposable disposables)
        {
            this.OneWayBind(accountViewModel,
                v => v.Kind,
                vm => vm._kindIcon.Kind,
                ConvertAccountKindToPackIconKind)
                .DisposeWith(disposables);

            this.OneWayBind(accountViewModel,
                v => v.Iban,
                vm => vm._ibanTextBlock.Text,
                IbanUtils.Format)
                .DisposeWith(disposables);

            this.OneWayBind(accountViewModel,
                v => v.Description,
                vm => vm._descriptionTextBlock.Text)
                .DisposeWith(disposables);

            this.OneWayBind(accountViewModel,
                v => v.CurrentValue,
                vm => vm._valueTextBlock.Text,
                s => s.ToString("C2"))
                .DisposeWith(disposables);
        }

        private PackIconKind ConvertAccountKindToPackIconKind(AccountKind kind)
        {
            switch (kind)
            {
                case AccountKind.Checking:
                    return PackIconKind.Money;

                case AccountKind.Saving:
                    return PackIconKind.Bank;

                case AccountKind.Locked:
                    return PackIconKind.Lock;

                default:
                    return PackIconKind.QuestionMark;
            }
        }
    }
}