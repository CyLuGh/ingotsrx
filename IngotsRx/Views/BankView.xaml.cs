﻿using IngotsRx.ViewModels;
using IngotsRx.Views.Infrastructure;
using ReactiveUI;
using System;
using System.Collections.Generic;
using System.Reactive.Disposables;
using System.Reactive.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace IngotsRx.Views
{
    public partial class BankView
    {
        public BankView()
        {
            InitializeComponent();

            this.WhenActivated(disposables =>
            {
                this.WhenAnyValue(v => v.ViewModel)
                    .Where(vm => vm != null)
                    .Do(x => PopulateFromViewModel(x, disposables))
                    .Subscribe()
                    .DisposeWith(disposables);
            });
        }

        private void PopulateFromViewModel(BankViewModel bankViewModel, CompositeDisposable disposables)
        {
            this.OneWayBind(bankViewModel,
                vm => vm.Name,
                v => v.BankTextBlock.Text)
                .DisposeWith(disposables);
            
            this.OneWayBind(bankViewModel,
                    vm => vm.TotalValue,
                    v => v.TotalTextBlock.Text,
                    v => v.ToString("C2"))
                .DisposeWith(disposables);

            this.OneWayBind(bankViewModel,
                vm => vm.Accounts,
                v => v.AccountsListBox.ItemsSource)
                .DisposeWith(disposables);

            this.Bind(bankViewModel,
                vm => vm.SelectedAccount,
                v => v.AccountsListBox.SelectedItem)
                .DisposeWith(disposables);
        }
    }
}