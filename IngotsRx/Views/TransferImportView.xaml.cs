﻿using System.Reactive.Disposables;
using System.Reactive.Linq;
using IngotsRx.ViewModels;
using IngotsRx.Views.Converters;
using ReactiveUI;

namespace IngotsRx.Views
{
    public partial class TransferImportView 
    {
        public TransferImportView()
        {
            InitializeComponent();
            
            this.WhenActivated(disposables =>
            {
                this.WhenAnyValue(o => o.ViewModel)
                    .WhereNotNull()
                    .Do( PopulateFromViewModel)
                    .SubscribeSafe()
                    .DisposeWith(disposables);
            });
        }
        
        private void PopulateFromViewModel(TransferViewModel transferViewModel)
        {
            DateTextBlock.Text = transferViewModel.Date.ToString("yyyy-MM-dd");
            var target = IdAccountConverter.Instance.Convert(transferViewModel.TargetId, typeof(AccountViewModel), null, null) as AccountViewModel;
            TargetTextBlock.Text = target?.Description ?? string.Empty;
            DescriptionTextBlock.Text = transferViewModel.Description;
            ValueTextBlock.Text = transferViewModel.Value.ToString("C2");
        }
    }
}