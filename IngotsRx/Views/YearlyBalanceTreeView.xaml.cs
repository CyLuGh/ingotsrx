﻿using System;
using System.Reactive.Disposables;
using System.Reactive.Linq;
using System.Windows.Controls;
using IngotsRx.ViewModels;
using ReactiveUI;

namespace IngotsRx.Views
{
    public partial class YearlyBalanceTreeView 
    {
        public YearlyBalanceTreeView()
        {
            InitializeComponent();
            
            this.WhenActivated(disposables =>
            {
                this.WhenAnyValue(x => x.ViewModel)
                    .WhereNotNull()
                    .Do(vm => PopulateFromViewModel(vm, disposables))
                    .Subscribe()
                    .DisposeWith(disposables);
            });
        }
        
        private void PopulateFromViewModel(BalanceViewModel balanceViewModel, CompositeDisposable disposables)
        {
            this.OneWayBind(balanceViewModel,
                    vm => vm.Period,
                    v => v.PeriodTextBlock.Text,
                    d => d.ToString("yyyy"))
                .DisposeWith(disposables);
            
            this.OneWayBind(balanceViewModel,
                    vm => vm.Income,
                    v => v.IncomeTextBlock.Text,
                    d => d.ToString("C2"))
                .DisposeWith(disposables);
            
            this.OneWayBind(balanceViewModel,
                    vm => vm.Expense,
                    v => v.ExpenseTextBlock.Text,
                    d => d.ToString("C2"))
                .DisposeWith(disposables);
            
            this.OneWayBind(balanceViewModel,
                    vm => vm.Balance,
                    v => v.BalanceTextBlock.Text,
                    d => d.ToString("C2"))
                .DisposeWith(disposables);
        }
    }
}