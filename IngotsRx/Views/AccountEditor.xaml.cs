﻿using IngotsRx.ViewModels;
using ReactiveUI;
using ReactiveUI.Validation.Extensions;
using System;
using System.Linq;
using System.Reactive.Disposables;
using System.Reactive.Linq;

namespace IngotsRx.Views
{
    public partial class AccountEditor
    {
        public AccountEditor()
        {
            InitializeComponent();

            this.WhenActivated(disposables =>
            {
                this.WhenAnyValue(v => v.ViewModel)
                  .Where(vm => vm != null)
                  .Do(x => PopulateFromViewModel(x, disposables))
                  .Subscribe()
                  .DisposeWith(disposables);
            });
        }

        private void PopulateFromViewModel(AccountViewModel accountViewModel, CompositeDisposable disposables)
        {
            this.Bind(accountViewModel,
                vm => vm.Bank,
                v => v._textBank.Text)
                .DisposeWith(disposables);

            this.Bind(accountViewModel,
                vm => vm.Iban,
                v => v._textIban.Text)
                .DisposeWith(disposables);
            this.BindValidation(accountViewModel, vm => vm.IbanRule, v => v._errorIbanTextBlock.Text)
                .DisposeWith(disposables);

            this.Bind(accountViewModel,
                vm => vm.Bic,
                v => v._textBic.Text)
                .DisposeWith(disposables);

            this.Bind(accountViewModel,
                vm => vm.Description,
                v => v._textDescription.Text)
                .DisposeWith(disposables);

            this.Bind(accountViewModel,
                vm => vm.Kind,
                v => v._cmbKind.SelectedItem)
                .DisposeWith(disposables);

            this.Bind(accountViewModel,
                vm => vm.StartValueInput,
                v => v._textStartValue.Text)
                .DisposeWith(disposables);
            this.BindValidation(accountViewModel, vm => vm.StartValueRule, v => v._errorStartValueTextBlock.Text)
                .DisposeWith(disposables);
        }
    }
}