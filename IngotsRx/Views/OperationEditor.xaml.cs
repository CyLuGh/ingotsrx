﻿using IngotsRx.ViewModels;
using MaterialDesignThemes.Wpf;
using ReactiveUI;
using Splat;
using System;
using System.Reactive;
using System.Reactive.Disposables;
using System.Reactive.Linq;

namespace IngotsRx.Views
{
    public partial class OperationEditor
    {
        public OperationEditor()
        {
            InitializeComponent();

            this.WhenActivated(disposables =>
            {
                this.WhenAnyValue(v => v.ViewModel)
                  .WhereNotNull()
                  .Do(x => PopulateFromViewModel(x, disposables))
                  .Subscribe()
                  .DisposeWith(disposables);

                this.WhenAnyValue(v => v.ViewModel)
                  .WhereNotNull()
                  .Select(vm =>
                  {
                      var viewLocator = Locator.Current.GetService<IViewLocator>();
                      var view = viewLocator.ResolveView(vm, "Editor");
                      view.ViewModel = vm;
                      return view;
                  })
                  .BindTo(_specRoutedViewHost, x => x.Content)
                  .DisposeWith(disposables);
            });
        }

        private void PopulateFromViewModel(OperationViewModel ovm, CompositeDisposable disposables)
        {
            ovm.ValidateInteraction
                .RegisterHandler(context =>
                {
                    DialogHost.CloseDialogCommand.Execute(true, null);
                    context.SetOutput(Unit.Default);
                })
                .DisposeWith(disposables);

            this.Bind(ovm,
                vm => vm.Date,
                v => v._datePicker.SelectedDate)
                .DisposeWith(disposables);

            this.Bind(ovm,
              vm => vm.ValueInput,
              v => v._textValue.Text)
              .DisposeWith(disposables);

            this.Bind(ovm,
                vm => vm.Description,
                v => v._textDescription.Text)
                .DisposeWith(disposables);

            this.Bind(ovm,
                vm => vm.IsExecuted,
                v => v._executedToggle.IsChecked)
                .DisposeWith(disposables);

            this.OneWayBind(ovm,
                vm => vm.ErrorMessage,
                v => v._txtError.Text)
                .DisposeWith(disposables);
            this.OneWayBind(ovm,
                vm => vm.ErrorMessage,
                v => v._txtError.Visibility,
                s => string.IsNullOrEmpty(s) ? System.Windows.Visibility.Collapsed : System.Windows.Visibility.Visible)
                .DisposeWith(disposables);

            this.BindCommand(ovm,
                vm => vm.ValidateCommand,
                v => v._bSave)
                .DisposeWith(disposables);
        }
    }
}