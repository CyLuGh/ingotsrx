﻿using IngotsRx.ViewModels;
using MaterialDesignThemes.Wpf;
using ReactiveUI;
using System;
using System.Reactive.Disposables;
using System.Reactive.Linq;

namespace IngotsRx.Views
{
    public partial class EventLogView
    {
        public EventLogView()
        {
            InitializeComponent();

            this.WhenActivated(disposables =>
            {
                this.WhenAnyValue(x => x.ViewModel)
                    .WhereNotNull()
                    .Do(x => PopulateFromViewModel(x, disposables))
                    .Subscribe()
                    .DisposeWith(disposables);
            });
        }

        private void PopulateFromViewModel(EventLogViewModel viewModel, CompositeDisposable disposables)
        {
            this.OneWayBind(viewModel,
                vm => vm.Kind,
                v => v._iconKind.Kind,
                k => k switch
                     {
                         EventKind.Info => PackIconKind.InfoCircle,
                         EventKind.Error => PackIconKind.Error,
                         EventKind.Warn => PackIconKind.Exclamation,
                         _ => PackIconKind.QuestionMark,
                     }
                )
                .DisposeWith(disposables);

            this.OneWayBind(viewModel,
                vm => vm.TimeStamp,
                v => v._txtTimeStamp.Text,
                d => d.ToString("yyyy-MM-dd HH:mm:ss"))
                .DisposeWith(disposables);

            this.OneWayBind(viewModel,
                vm => vm.Message,
                v => v._txtMessage.Text)
                .DisposeWith(disposables);
        }
    }
}