﻿using System;
using System.Linq;
using System.Reactive.Disposables;
using System.Reactive.Linq;
using System.Windows.Controls;
using IngotsRx.ViewModels;
using ReactiveUI;

namespace IngotsRx.Views
{
    public partial class OperationsImportView
    {
        public OperationsImportView()
        {
            InitializeComponent();

            this.WhenActivated(disposables =>
            {
                this.WhenAnyValue(o => o.ViewModel)
                    .WhereNotNull()
                    .Do(vm => PopulateFromViewModel(vm, disposables))
                    .SubscribeSafe()
                    .DisposeWith(disposables);
            });
        }

        private void PopulateFromViewModel(OperationsImportViewModel operationsImportViewModel,
            CompositeDisposable disposables)
        {
            this.OneWayBind(operationsImportViewModel,
                    vm => vm.UnselectedOperations,
                    v => v.UnselectedListBox.ItemsSource)
                .DisposeWith(disposables);

            this.OneWayBind(operationsImportViewModel,
                    vm => vm.SelectedOperations,
                    v => v.SelectedListBox.ItemsSource)
                .DisposeWith(disposables);

            this.BindCommand(operationsImportViewModel,
                    vm => vm.SelectAllCommand,
                    v => v.SelectAllButton)
                .DisposeWith(disposables);

            this.BindCommand(operationsImportViewModel,
                    vm => vm.SelectNoneCommand,
                    v => v.SelectNoneButton)
                .DisposeWith(disposables);
            
            this.BindCommand(operationsImportViewModel,
                    vm => vm.SelectCommand,
                    v => v.SelectButton)
                .DisposeWith(disposables);

            this.BindCommand(operationsImportViewModel,
                    vm => vm.UnselectCommand,
                    v => v.UnselectButton)
                .DisposeWith(disposables);

            this.BindCommand(operationsImportViewModel,
                    vm => vm.CancelCommand,
                    v => v.CancelButton)
                .DisposeWith(disposables);

            this.BindCommand(operationsImportViewModel,
                    vm => vm.ImportCommand,
                    v => v.ImportButton)
                .DisposeWith(disposables);

            operationsImportViewModel.SelectInteraction.RegisterHandler(context =>
                    context.SetOutput(UnselectedListBox.SelectedItems.OfType<OperationViewModel>()))
                .DisposeWith(disposables);
            operationsImportViewModel.UnselectInteraction.RegisterHandler(context =>
                    context.SetOutput(SelectedListBox.SelectedItems.OfType<OperationViewModel>()))
                .DisposeWith(disposables);
        }
    }
}