﻿using IngotsRx.ViewModels;
using ReactiveUI;
using System;
using System.Collections.Generic;
using System.Reactive.Disposables;
using System.Reactive.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace IngotsRx.Views
{
    public partial class LoggingView
    {
        public LoggingView()
        {
            InitializeComponent();

            this.WhenActivated(disposables =>
            {
                this.WhenAnyValue(x => x.ViewModel)
                    .WhereNotNull()
                    .Do(x => PopulateFromViewModel(x, disposables))
                    .Subscribe()
                    .DisposeWith(disposables);
            });
        }

        private void PopulateFromViewModel(LoggingViewModel viewModel, CompositeDisposable disposables)
        {
            this.OneWayBind(viewModel,
                vm => vm.Logs,
                v => v._lbEvents.ItemsSource)
                .DisposeWith(disposables);
        }
    }
}