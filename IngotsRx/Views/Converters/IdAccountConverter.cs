﻿using IngotsRx.ViewModels;
using ReactiveUI;
using Splat;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

namespace IngotsRx.Views.Converters
{
    public sealed class IdAccountConverter : IValueConverter
    {
        public static IdAccountConverter Instance { get; } = new IdAccountConverter();

        public Dictionary<long, AccountViewModel> Accounts { get; } = new Dictionary<long, AccountViewModel>();

        private IdAccountConverter()
        {
        }

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is long id && Accounts.TryGetValue(id, out var account))
                return account;

            return DependencyProperty.UnsetValue;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is AccountViewModel avm)
                return avm.Id;

            return DependencyProperty.UnsetValue;
        }
    }
}