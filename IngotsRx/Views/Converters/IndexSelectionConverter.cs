﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;
using System.Windows;
using System.Windows.Data;

namespace IngotsRx.Views.Converters
{
    public sealed class IndexSelectionConverter : IValueConverter
    {
        public static IndexSelectionConverter Instange { get; } = new IndexSelectionConverter();

        private IndexSelectionConverter()
        {
        }

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is int selected && parameter is int desired)
                return selected == desired;

            return DependencyProperty.UnsetValue;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is bool check && parameter is int desired && check)
                return desired;

            return DependencyProperty.UnsetValue;
        }
    }
}