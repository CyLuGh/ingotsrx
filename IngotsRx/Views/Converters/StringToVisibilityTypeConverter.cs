﻿using ReactiveUI;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;
using System.Windows;
using System.Windows.Data;

namespace IngotsRx.Views.Converters
{
    public class StringToVisibilityTypeConverter : IBindingTypeConverter
    {
        public int GetAffinityForObjects(Type fromType, Type toType)
        {
            if (fromType == typeof(string) && toType == typeof(Visibility))
            {
                return 10;
            }

            if (fromType == typeof(Visibility) && toType == typeof(string))
            {
                return 10;
            }

            return 0;
        }

        public bool TryConvert(object from, Type toType, object conversionHint, out object result)
        {
            result = Visibility.Collapsed;
            return true;
        }
    }
}