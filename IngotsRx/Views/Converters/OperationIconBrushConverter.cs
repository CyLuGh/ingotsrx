﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;
using System.Windows.Media;
using IngotsRx.ViewModels;

namespace IngotsRx.Views.Converters
{
    public sealed class OperationIconBrushConverter : IValueConverter
    {
        public static OperationIconBrushConverter Instance { get; } = new OperationIconBrushConverter();

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is OperationViewModel ovm)
            {
                return ovm.Value < 0 ? Brushes.IndianRed : Brushes.SeaGreen;
            }

            return DependencyProperty.UnsetValue;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
            => DependencyProperty.UnsetValue;
    }
}