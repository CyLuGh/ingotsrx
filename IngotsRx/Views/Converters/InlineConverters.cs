﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;

namespace IngotsRx.Views.Converters
{
    public static class InlineConverters
    {
        public static Visibility StringToVisibilityConversion(this string input)
            => string.IsNullOrWhiteSpace(input) ? Visibility.Collapsed : Visibility.Visible;
    }
}