﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;
using IngotsRx.ViewModels;
using MaterialDesignThemes.Wpf;

namespace IngotsRx.Views.Converters
{
    public sealed class OperationIconConverter : IValueConverter
    {
        public static OperationIconConverter Instance { get; } = new OperationIconConverter();

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is TransferViewModel)
                return PackIconKind.ShuffleVariant;

            if (value is TransactionViewModel transaction)
                return transaction.Value < 0
                    ? PackIconKind.ArrowDownBoldBoxOutline
                    : PackIconKind.ArrowUpBoldBoxOutline;

            return DependencyProperty.UnsetValue;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
            => DependencyProperty.UnsetValue;
    }
}