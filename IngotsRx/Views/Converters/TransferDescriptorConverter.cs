﻿using IngotsRx.ViewModels;
using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

namespace IngotsRx.Views.Converters
{
    public sealed class TransferDescriptorConverter : IValueConverter
    {
        public static TransferDescriptorConverter Instance { get; } = new TransferDescriptorConverter();

        private TransferDescriptorConverter()
        {
        }

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is TransferViewModel tvm)
            {
                if (targetType == typeof(string))
                    return tvm.Value < 0 ? "To:" : "From:";
            }

            return DependencyProperty.UnsetValue;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
            => DependencyProperty.UnsetValue;
    }
}