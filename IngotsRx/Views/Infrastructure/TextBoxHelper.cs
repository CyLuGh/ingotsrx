﻿using System.Windows;
using System.Windows.Controls;

namespace IngotsRx.Views.Infrastructure
{
    public class TextBoxHelper
    {
        public static bool GetSelectAll(FrameworkElement frameworkElement)
        {
            return (bool)frameworkElement.GetValue(SelectAllProperty);
        }

        public static void SetSelectAll(FrameworkElement frameworkElement, bool value)
        {
            frameworkElement.SetValue(SelectAllProperty, value);
        }

        public static readonly DependencyProperty SelectAllProperty =
                 DependencyProperty.RegisterAttached("SelectAll",
                    typeof(bool), typeof(TextBoxHelper),
                    new FrameworkPropertyMetadata(false, OnSelectAllChanged));

        private static void OnSelectAllChanged
                   (DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var frameworkElement = d as FrameworkElement;
            if (frameworkElement == null) return;

            if (e.NewValue is bool == false) return;

            if ((bool)e.NewValue)
            {
                frameworkElement.GotFocus += SelectAll;
                frameworkElement.PreviewMouseDown += IgnoreMouseButton;
            }
            else
            {
                frameworkElement.GotFocus -= SelectAll;
                frameworkElement.PreviewMouseDown -= IgnoreMouseButton;
            }
        }

        private static void SelectAll(object sender, RoutedEventArgs e)
        {
            var frameworkElement = e.OriginalSource as FrameworkElement;
            if (frameworkElement is TextBox tb)
                tb.SelectAll();
            else if (frameworkElement is PasswordBox pd)
                pd.SelectAll();
        }

        private static void IgnoreMouseButton
                (object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            var frameworkElement = sender as FrameworkElement;
            if (frameworkElement == null || frameworkElement.IsKeyboardFocusWithin) return;
            e.Handled = true;
            frameworkElement.Focus();
        }
    }
}