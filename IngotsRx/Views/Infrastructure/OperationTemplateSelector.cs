﻿using IngotsRx.ViewModels;
using System.Windows;
using System.Windows.Controls;

namespace IngotsRx.Views.Infrastructure
{
    public class OperationTemplateSelector : DataTemplateSelector
    {
        public DataTemplate TransactionDataTemplate { get; set; }
        public DataTemplate TransferDataTemplate { get; set; }

        public override DataTemplate SelectTemplate(object item, DependencyObject container)
            => item is TransactionViewModel ? TransactionDataTemplate : TransferDataTemplate;
    }

    public class TargetAccountTemplateSelector : DataTemplateSelector
    {
        public DataTemplate SelectedDataTemplate { get; set; }
        public DataTemplate InDropDownDataTemplate { get; set; }

        public override DataTemplate SelectTemplate(object item, DependencyObject container)
            => container.GetVisualParent<ComboBoxItem>() != null ? InDropDownDataTemplate : SelectedDataTemplate;
    }
}