﻿using IngotsRx.ViewModels;
using IngotsRx.Views.Infrastructure;
using LanguageExt;
using MaterialDesignExtensions.Controls;
using MaterialDesignThemes.Wpf;
using OxyPlot;
using OxyPlot.Axes;
using ReactiveUI;
using System;
using System.Reactive.Disposables;
using System.Reactive.Linq;
using System.Windows;
using System.Windows.Media;
using DynamicData;

namespace IngotsRx.Views
{
    public partial class AccountsView
    {
        public AccountsView()
        {
            InitializeComponent();
            _plotView.Configure();

            this.WhenActivated(disposables =>
            {
                this.WhenAnyValue(v => v.ViewModel)
                    .Where(vm => vm != null)
                    .Do(x => PopulateFromViewModel(x, disposables))
                    .Subscribe()
                    .DisposeWith(disposables);
            });
        }

        private void PopulateFromViewModel(AccountsViewModel accountsViewModel, CompositeDisposable disposables)
        {
            this.OneWayBind(accountsViewModel,
                    vm => vm.Banks,
                    v => v._banksList.ItemsSource)
                .DisposeWith(disposables);

            this.OneWayBind(accountsViewModel,
                    vm => vm.SelectedAccount,
                    v => v._selectedAccountGroupBox.Header,
                    a => a?.Description ?? string.Empty)
                .DisposeWith(disposables);

            this.BindCommand(accountsViewModel,
                    vm => vm.AddAccountCommand,
                    v => v._addAccountButton)
                .DisposeWith(disposables);

            this.BindCommand(accountsViewModel,
                    vm => vm.EditAccountCommand,
                    v => v._editAccountButton,
                    accountsViewModel.WhenAnyValue(o => o.SelectedAccount))
                .DisposeWith(disposables);

            this.OneWayBind(accountsViewModel,
                    vm => vm.Operations,
                    v => v._operationsDataGrid.ItemsSource)
                .DisposeWith(disposables);

            this.Bind(accountsViewModel,
                    vm => vm.SelectedOperation,
                    v => v._operationsDataGrid.SelectedItem)
                .DisposeWith(disposables);

            this.Bind(accountsViewModel,
                    vm => vm.SearchText,
                    v => v._txtSearch.Text)
                .DisposeWith(disposables);

            this.BindCommand(accountsViewModel,
                    vm => vm.AddTransactionCommand,
                    v => v._addTransactionButton)
                .DisposeWith(disposables);

            this.BindCommand(accountsViewModel,
                    vm => vm.AddTransferCommand,
                    v => v._addTransferButton)
                .DisposeWith(disposables);

            this.BindCommand(accountsViewModel,
                    vm => vm.AskDeleteOperationCommand,
                    v => v._deleteOperationButton,
                    accountsViewModel.WhenAnyValue(o => o.SelectedOperation))
                .DisposeWith(disposables);

            this.BindCommand(accountsViewModel,
                    vm => vm.EditOperationCommand,
                    v => v._editOperationButton,
                    accountsViewModel.WhenAnyValue(o => o.SelectedOperation))
                .DisposeWith(disposables);

            this.BindCommand(accountsViewModel,
                    vm => vm.ToggleExecutionCommand,
                    v => v._toggleExecutionButton)
                .DisposeWith(disposables);

            this.OneWayBind(accountsViewModel,
                    vm => vm.DrawnMonths,
                    v => v._runDrawnMonth.Text)
                .DisposeWith(disposables);

            this.Bind(accountsViewModel,
                    vm => vm.DrawnMonths,
                    v => v._sliderDrawnMonths.Value)
                .DisposeWith(disposables);

            this.BindCommand(accountsViewModel,
                    vm => vm.PickImportFileCommand,
                    v => v._importButton)
                .DisposeWith(disposables);

            accountsViewModel.EditAccountInteraction.RegisterHandler(async context =>
                             {
                                 var avm = context.Input;
                                 var res = (bool) await DialogHost.Show(new AccountEditor {ViewModel = avm});
                                 context.SetOutput(res ? Option<AccountViewModel>.Some(avm) : Option<AccountViewModel>.None);
                             })
                             .DisposeWith(disposables);

            accountsViewModel.EditTransactionInteraction.RegisterHandler(async context =>
                             {
                                 var tvm = context.Input;
                                 var res = (bool) await DialogHost.Show(new OperationEditor {ViewModel = tvm});
                                 context.SetOutput(res ? Option<TransactionViewModel>.Some(tvm) : Option<TransactionViewModel>.None);
                             })
                             .DisposeWith(disposables);

            accountsViewModel.EditTransferInteraction.RegisterHandler(async context =>
                             {
                                 var tvm = context.Input;
                                 var res = (bool) await DialogHost.Show(new OperationEditor {ViewModel = tvm});
                                 context.SetOutput(res ? Option<TransferViewModel>.Some(tvm) : Option<TransferViewModel>.None);
                             })
                             .DisposeWith(disposables);

            accountsViewModel.AskDeleteOperationInteraction.RegisterHandler(async context =>
                             {
                                 var ovm = context.Input;

                                 Func<OperationViewModel, string> showType = ovm =>
                                     ovm is TransactionViewModel ? "transaction" : "transfer";

                                 var qdvm = new QuestionDialogViewModel
                                            {
                                                Title = "Delete operation",
                                                Message =
                                                    $"Are you sure you wish to delete this {showType(ovm)} of {ovm.Value:C2} on {ovm.Date:yyyy-MM-dd}?",
                                                AcceptText = "DELETE OPERATION",
                                                CancelText = "KEEP OPERATION"
                                            };
                                 await DialogHost.Show(new QuestionDialogView {ViewModel = qdvm});
                                 context.SetOutput(qdvm.Answer == QuestionAnswer.Accept
                                     ? Option<OperationViewModel>.Some(ovm)
                                     : Option<OperationViewModel>.None);
                             })
                             .DisposeWith(disposables);

            accountsViewModel.ShowChartInteraction.RegisterHandler(context =>
                             {
                                 var accentBrush = (Brush) _plotView.TryFindResource("SecondaryAccentBrush") ?? Brushes.DarkGray;

                                 _plotView.Model.Series.Clear();
                                 _plotView.Model.AddStepLineSeries(context.Input, accentBrush);
                                 _plotView.InvalidatePlot(true);
                                 context.SetOutput(System.Reactive.Unit.Default);
                             })
                             .DisposeWith(disposables);

            accountsViewModel.PickImportFileInteraction.RegisterHandler(async context =>
                             {
                                 var filePath = string.Empty;
                                 var dialogArgs = new OpenFileDialogArguments()
                                                  {
                                                      Width = 800,
                                                      Height = 600,
                                                      Filters = "CSV files|*.csv;*.txt"
                                                  };

                                 var result = await OpenFileDialog.ShowDialogAsync(((MainWindow) Window.GetWindow(this))._dialogHost,
                                     dialogArgs);
                                 if (result.Confirmed)
                                     filePath = result.File;

                                 context.SetOutput(filePath);
                             })
                             .DisposeWith(disposables);

            accountsViewModel.ChooseOperationsInteraction.RegisterHandler(async context =>
                             {
                                 var res = (bool) await DialogHost.Show(new OperationsImportView {ViewModel = context.Input});
                                 if (res)
                                     context.SetOutput(Option<OperationsImportViewModel>.Some(context.Input));
                                 else
                                     context.SetOutput(Option<OperationsImportViewModel>.None);
                             })
                             .DisposeWith(disposables);
        }
    }
}