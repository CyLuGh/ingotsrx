﻿using IngotsRx.ViewModels;
using ReactiveUI;
using System;
using System.Collections.Generic;
using System.Reactive.Disposables;
using System.Reactive.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace IngotsRx.Views
{
    /// <summary>
    /// Interaction logic for TransactionEditorPartView.xaml
    /// </summary>
    public partial class TransactionEditorPartView
    {
        public TransactionEditorPartView()
        {
            InitializeComponent();

            this.WhenActivated(disposables =>
            {
                this.WhenAnyValue(v => v.ViewModel)
                  .WhereNotNull()
                  .Do(x => PopulateFromViewModel(x, disposables))
                  .Subscribe()
                  .DisposeWith(disposables);
            });
        }

        private void PopulateFromViewModel(TransactionViewModel tvm, CompositeDisposable disposables)
        {
            this.Bind(tvm,
                vm => vm.Category,
                v => v._categoryCombo.Text)
                .DisposeWith(disposables);

            this.OneWayBind(tvm,
                vm => vm.AvailableCategories,
                v => v._categoryCombo.ItemsSource)
                .DisposeWith(disposables);

            this.Bind(tvm,
                vm => vm.SubCategory,
                v => v._subcategoryCombo.Text)
                .DisposeWith(disposables);

            this.OneWayBind(tvm,
                vm => vm.AvailableSubcategories,
                v => v._subcategoryCombo.ItemsSource)
                .DisposeWith(disposables);

            this.Bind(tvm,
                vm => vm.Shop,
                v => v._shopCombo.Text)
                .DisposeWith(disposables);

            this.OneWayBind(tvm,
                vm => vm.AvailableShops,
                v => v._shopCombo.ItemsSource)
                .DisposeWith(disposables);

            this.OneWayBind(tvm,
                vm => vm.Category,
                v => v._subcategoryCombo.IsEnabled,
                c => !string.IsNullOrWhiteSpace(c))
                .DisposeWith(disposables);
        }
    }
}